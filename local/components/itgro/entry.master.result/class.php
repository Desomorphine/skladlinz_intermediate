<?php

use Focus\Component;
use Focus\Storage;
use Itgro\User;

class ResultComponent extends Component
{
	function save ()
	{
		$fullName = Storage::get('last_name') . " " . Storage::get('first_name') . " " . Storage::get('second_name');
		$fullName = trim($fullName);

		$timestamp = time();

		$arElement =
		[
			'NAME' => $fullName . ' - ' . date('d-m-Y H:i:s'),
			'PROPERTY_VALUES' =>
			[
				'USER' => User::getInstance()->GetID(),
				'REFINEMENTS' => Storage::get('clarifications'),
				'SALON' => Storage::get(''),
				'DOCTOR_TYPE' => Storage::get(''),
				'DATE' => Storage::get(''),
				'DURATION' => Storage::get('duration'),
				'NAME_OF_PATIENT' => $fullName,
				'PHONE' => Storage::get(''),
				'EMAIL' => Storage::get(''),
				'COMMENT' => Storage::get(''),
				'DOCTOR_NAME' => Storage::get(''),
			]
		];
	}

	function get ($id) { }

	function all () { }

	function action ()
	{
		$this->save ();
	}
}
