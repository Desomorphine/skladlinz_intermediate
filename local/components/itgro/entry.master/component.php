<?
use Itgro\App;
use Itgro\Request\Request;

(defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

/* @global CMain $APPLICATION */
/* @var CBitrixComponent $this */
/* @var array $arParams */

try
{
	if (!CModule::IncludeModule('entry'))
	{
		throw new Exception();
	}

	$arResult = array();

	$arParams['SEF_FOLDER'] = 'module';

	$arUrlTemplates = array(
		'index' => '/',
		'step' => '#step_name#/',
		'id' => '#step_name#/#id#'
	);

	$arComponentVariables = array();

	$arVariables = array();
	$arVariableAliases = array();

	$engine = new CComponentEngine($this);

	$componentPage = $engine->guessComponentPath(
		$arParams['SEF_FOLDER'],
		$arUrlTemplates,
		$arVariables
	);

	CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

	$componentPage = '';

	if (array_key_exists('step_name', $arVariables))
	{
		$componentPage = $arVariables['step_name'];
	}

	$arResult = array(
		'URL_TEMPLATES' => $arUrlTemplates,
		'VARIABLES' => $arVariables,
		'ALIASES' => $arVariableAliases
	);

	$this->IncludeComponentTemplate($componentPage);
}
catch (Exception $e)
{
	if (App::ajax())
	{
		App::json_exception($e);
	}
	else
	{
		ShowError($e->getMessage());
	}
}

