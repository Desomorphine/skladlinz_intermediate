<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="/assets/css/main.css">
	<script src="/es5-shim/es5-shim.js" type="text/javascript"></script>

</head>
<body>

	<a href="#service" class="js-modal" data-modal="#modal">Модальное окно</a> <br>

	<div class="imodal" id="modal">
		<div class="imodal__box">
			<div class="imodal__close js-imodal-close"></div>
			<div class="imodal__title">
				Заполнив данную форму, вы можете записаться на прием к врачу по нужной вам услуге - и в удобное для вас время!
			</div>
			<ol class="menu js-menu">
				<!-- Меню  -->
			</ol>
			<form class="form js-preloader" id="container">
				
				<div class="imodal__body">

				</div>
				<div class="imodal__footer">
					<!--  Кнопки  -->
				</div>
			</form>
		</div>
	</div>

	<script type="text/template" id="menu">
		<% _.each(menu, function(item) { %>
			<li class="menu__item <% if (active === item.url) { %> active <% } %>"><span class="menu__text"><%= item.name %></span></li>
		<% }) %>
	</script>

	<script type="text/template" id="menu-control">
		<% if (typeof control.next !== 'undefined') { %>
			<input type="submit" value="<%= control.next.name %>" class="btn btn--primary float-right js-menu-control-next">
		<% } %>
		<% if (typeof control.prev !== 'undefined') { %>
			<a href="<%= control.prev.url %>" class="btn js-menu-control-prev"><%= control.prev.name %></a>
		<% } %>
		<% if (_.isEmpty(control)) { %>
			<a href="#comments" class="btn btn--primary float-right">Записаться еще раз</a>
			<a href="/" class="link float-right">На главную</a>
		<% } %>
	</script>

	<script type="text/template" id="services">
		<div class="title"><span class="title__number">1</span>Какая вам услуга нужна?</div>
		<div class="message-error"></div>
		<div class="form__group form__group--highlight">
			<label for="" class="form__group-label">Услуга</label>
			<select name="service" class="form__control form__control--small js-select js-service-select">

				<% _.each(services, function(service) { %>
					<option value="<%= service.id %>"><%= service.name %></option>
				<% }) %>

			</select>

			<span class="js-detail-link">
				<!--  Ссылка на описание  -->
			</span>
		</div>
		<div class="form__group-title">Укажите дополнительные сведения о себе:</div>
			<div class="form__groups js-questions">
				<!--  Вопросы  -->
			</div>
		<div class="form__group form__group--highlight js-duration">
			<!--  Время  -->
		</div>
	</script>

	<script type="text/template" id="detail-link">
		<% if (detailLink) { %>
			<a href="<%= detailLink %>">Подробнее об услуге.</a>
		<% } %>
	</script>

	<script type="text/template" id="question">
		<label for="" class="form__group-label"><%= question.name  %></label>
		<div class="form__group-field">
			<% _.each(question.answers, function(answer) { %>
				<div class="radio radio--large">
					<input
						type="radio"
						name="<%= question.input_name %>"
						value="<%= answer.id %>"
						id="answer_<%= answer.id %>"
						<% if (question.answer_check === answer.id) { %>
						checked
						<% } %>
						class="radio__input">
					<label for="answer_<%= answer.id %>" class="radio__label"><%= answer.name %></label>
					<% if (answer.clarification) { %>
						<div class="form__field form__field--helper">
							<div class="form__field-helper"><%= answer.clarification %></div>
						</div>
					<% } %>
				</div>
			<% }) %>
		</div>
	</script>

	<script type="text/template" id="duration">
		<b>Продолжительность: <span class="highlight js-duration"><%= duration %> минут</span></b>
	</script>

	<script type="text/template" id="salon">
		<div class="title"><span class="title__number">2</span>В какой салон вам удобно прийти?</div>
		<div class="form__group form__group--highlight">
			<label for="" class="form__group-label">Салон</label>
			<select name="salon" class="form__control form__control--small js-select js-salon-select">

				<% _.each(salons, function(salon) { %>
					<option value="<%= salon.id %>"><%= salon.address %></option>
				<% }) %>

			</select>
		</div>
		<div class="form__group">
			<div class="salon">
				<div class="salon__map" id="map">

				</div>
				<ul class="salon__details js-salon-detail">

				</ul>
			</div>
		</div>
	</script>

	<script type="text/template" id="salon-detail">
		<% if (salon.address) { %>
			<li class="salon__detail">
				<b>Адрес</b> <br>
				<%= salon.address %>
			</li>
		<% } %>
		<% if (salon.phone) { %>
			<li class="salon__detail">
				<b>Телефон</b> <br>
				<%= salon.phone %>
			</li>
		<% } %>	
		<% if (salon.time_work) { %>	
			<li class="salon__detail">
				<b>Время работы</b> <br>
				<%= salon.time_work %>
			</li>
		<% } %>
		<% if (salon.trams || salon.buses || salon.cabs) { %>
			<li class="salon__detail">
				<b>Как добраться</b> <br>
				<% if (salon.trams) { %>
					Трамваи <%= salon.trams %> <br>
				<% } %>
				<% if (salon.buses) { %>
					Автобусы <%= salon.buses %> <br>
				<% } %>
				<% if (salon.cabs) { %>
					Маршрутки <%= salon.cabs %>
				<% } %>
			</li>
		<% } %>		
	</script>

	<script type="text/template" id="timetable">
		<div class="title"><span class="title__number">3</span>В какой день и время вам удобно прийти?</div>
		<div class="message-error"></div>
		<div class="client-info">
			<div class="client-info__title">Вы выбрали</div>
			<b>Услуга:</b> <%= info.services %>, 
			<b>Врач:</b> <%= info.doctor %>, 
			<b>Салон:</b> <%= info.salon %> <br>
			Для оказания услуги потребуется: <b><%= info.duration %> минут</b>	
		</div>
		<div class="form__group form__group--highlight">
			<div class="form__group js-calendar">

			</div>
			<div class="form__group js-time">

			</div>
			<div class="form__group text">
				<div class="form__group-left">
					Стоимость: <%= info.cost %> рублей
				</div>
				<div class="form__group-right">		
					<p>Серым показано недоступное время, синим  –  доступное для начала приема, с учетом продолжительности приема у врача.</p>
					<p>Если среди свободных временных интервалов нет удобного для вас  –  вы можете поменять дату приема, или салон, вернувшись к предыдущим шагам.</p>
				</div>
			</div>
		</div>
	</script>

	<script type="text/template" id ="calendar">
		<div class="form__group">
			Укажите или выберите день приема
			<input type="text" class="form__control form__control--date" name="date" value="<%= _.findWhere(calendar, {active: true}).date %>">
		</div>
		<div class="form__group form__group--timetable">
			<div class="calendar">
				<div class="calendar__control calendar__control--next"></div>
				<div class="calendar__control calendar__control--prev"></div>
				<div class="calendar__viewport">
					<ul class="calendar__list">
						
						<% _.each(calendar, function(day) { %>
							<li class="calendar__day <% if (day.active) { %> calendar__day--active <% } %>"><%= day.day %> <%= day.date %></li>
						<% }) %>

					</ul>
				</div>
			</div>
		</div>
	</script>

	<script type="text/template" id="time">
		<div class="form__group">
			Какое доступое время начала приема вам подходит? 
			<input type="text" class="form__control form__control--time" name="time" value="<%= _.findWhere(time, {active: true}).from %>">
		</div>
		<div class="form__group form__group--timetable">
			<div class="time">

				<% _.each(time, function(el) { %>
					<div class="time__el<% if (el.disable) { %> time__el--disable <% } %><% if (el.active) { %> time__el--active <% } %>"><%= el.from %></div>						
				<% }) %>

			</div>
		</div>
	</script>

	<script type="text/template" id="comments">
		<div class="title"><span class="title__number">4</span>Представьтесь пожалуйста</div>
		<div class="message-error"></div>
		<div class="form__group form__group--highlight form__group--contacts">
			<div class="form__group">
				<div class="form__field">
					<label for="" class="form__field-label">
						Фамилия<span class="highlight">*</span>:
					</label>
					<input type="text" class="form__control form__control--medium js-required" name="last_name" >
				</div>
				<div class="form__field">
					<label for="" class="form__field-label">
						Имя<span class="highlight">*</span>:
					</label>
					<input type="text" class="form__control form__control--medium js-required" name="first_name" >
				</div>
				<div class="form__field">
					<label for="" class="form__field-label">
						Отчество<span class="highlight">*</span>:
					</label>
					<input type="text" class="form__control form__control--medium js-required" name="second_name" >
				</div>
			</div>
			<div class="form__group">
				<div class="form__group-text">
					На эти контактные данные мы пришлем вам подтверждение об успешной записи, а так же напоминание за сутки перед приемом.
				</div>
				<div class="form__field">
					<label for="" class="form__field-label">
						Эл.Почта:
					</label>
					<input type="email" class="form__control form__control--medium" name="email">
				</div>
				<div class="form__field">
					<label for="" class="form__field-label">
						Телефон<span class="highlight">*</span>:
					</label>
					<input type="text" class="form__control form__control--code js-required" name="phone_code" placeholder="+7" >
					<input type="text" class="form__control form__control--prefix js-required" name="phone_prefix" placeholder="912" >
					<input type="text" class="form__control form__control--phone js-required" name="phone" placeholder="1234567" >
				</div>
			</div>
			<div class="form__group">
				<div class="form__field">
					Если нужно, напишите комментарий к заявке(необязательно):
					<textarea name="comment" class="form__control form__control--ta"></textarea>
				</div>
			</div>
		</div>
	</script>

	<script type="text/template" id="result">
		<% result = 'y' %>
		<% if (result === 'y') { %>
			<div class="result">
				<div class="result__title">
					Ваша заявка на прием к врачу отправлена
				</div>
				Пожалуйста, дождитесь получения SMS или e-mail с подтверждением записи – в противном случае, позвоните по телефону (343)123-45-67 для проверки успешности записи на прием.
			</div>
		<% } else { %>
			<div class="result result--error">
				<div class="result__title">Ваша заявка на прием к врачу не отправлена</div>
				Пожалуйста, обратитесь за помощью по телефону (343)123-45-67 для проверки успешности записи на прием.
			</div>
		<% } %>
	</script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/assets/vendor/jquery-v1.11.1.min.js"><\/script>')</script>
	<script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru_RU" type="text/javascript"></script>
	<script src="/assets/vendor/underscore-v1.7.0.min.js"></script>
	<script src="/assets/vendor/backbone-v1.1.2.min.js"></script>
	<script src="/assets/js/main.js"></script>
	<script src="/assets/js/backbone.js"></script>
</body>
</html>