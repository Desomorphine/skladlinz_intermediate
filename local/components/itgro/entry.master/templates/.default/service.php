<? (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

/* @global CMain $APPLICATION */
/* @var CBitrixComponent $component */
/* @var array $arParams */
/* @var array $arResult */
?>

<?
$APPLICATION->IncludeComponent(
	"itgro:entry.master.service",
	"",
	Array(
		'ID' => $arResult['VARIABLES']['id'],
		"TICKETS_PER_PAGE" => 10,
	),
	$component,
	array("HIDE_ICONS" => "Y")
);
