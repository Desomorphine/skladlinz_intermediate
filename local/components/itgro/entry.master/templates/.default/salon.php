<? (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

/* @global CMain $APPLICATION */
/* @var CBitrixComponent $component */
/* @var array $arParams */
/* @var array $arResult */
?>

<?

// $_REQUEST = array
// (
// 	"FILTER" => array(
// 		"DOCTOR_TYPE" => array(1, 2),
// 		"LICENSE" => "Y",
// 		"IS_HELD" => "Y"
// 	),
// 	"DURATION" => 60,
// 	"COST" => 900
// );

$APPLICATION->IncludeComponent(
	"itgro:entry.master.salon",
	"",
	Array(
		'ID' => $arResult['VARIABLES']['ID'],
		"DOCTOR_TYPE" => $_REQUEST["FILTER"]["DOCTOR_TYPE"],
		"IS_HElD" => $_REQUEST["FILTER"]["IS_HELD"],
		"LICENSE" => $_REQUEST["FILTER"]["LICENSE"],
		"DURATION" => $_REQUEST["DURATION"],
		"COST" => $_REQUEST["COST"],
	),
	$component,
	array("HIDE_ICONS" => "Y")
);
