<?php

use Focus\Storage;
use Focus\Component;
use Itgro\Request\Request;

class CommentsComponent extends Component
{
	function save ()
	{
		$fields = [
			'last_name' => ['required' => true],
			'first_name' => ['required' => true],
			'second_name' => ['required' => false],
			'email' => ['required' => false],
			'phone_code' => ['required' => true],
			'phone_prefix' => ['required' => true],
			'phone' => ['required' => true],
			'comment' => ['required' => false],
		];

		foreach ($fields as $field => $fieldInfo)
		{
			if (Request::put()->has($field))
			{
				Storage::put($field, Request::put()->get($field));
			}
			else if ($fieldInfo['required'])
			{
				$this->save_error = 'Заполните все обязательные поля';
				return false;
			}
		}

		return true;
	}

	function get ($id)
	{
		return [];
	}

	function all ()
	{
		return [];
	}
}
