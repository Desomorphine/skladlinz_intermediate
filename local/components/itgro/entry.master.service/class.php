<?php

use Focus\Component;
use Focus\Repository\Service as Service;
use Focus\Storage;
use Itgro\Request\Request;


class ServiceComponent extends Component
{
	function save()
	{
		$service = Request::put()->get('service');

		if(intval($service) == 0)
		{
			$this->save_error = 'Некорректные данные';
			return false;
		}

		$arService = Service::getByID($service);

		if($arService == false)
		{
			$this->save_error = 'Несуществующая услуга';
			return false;
		}

		Storage::put('service', $service);
		Storage::put('duration', $arService["DURATION"]);
		//Storage::put('cost', $arService["COST"]);

		$arClarifications = Service::getClarifications($service);

		Storage::clear('clarifications');

		if(is_array($arClarifications) && count($arClarifications) > 0)
		{
			foreach ($arClarifications as $arQuestion)
			{
				if(!Request::put()->has("question_".$arQuestion["ID"]))
				{
					$this->save_error = 'Не выбрано ни одного ответа';
					return false;
				}

				$intChosenClarification = Request::put()->get("question_".$arQuestion["ID"]);

				if(!array_key_exists($intChosenClarification, $arQuestion["ANSWERS"]))
				{
					$this->save_error = '??';
					return false;
				}

				Storage::push('clarifications', $intChosenClarification);

				if(intval($arQuestion["ANSWERS"][$intChosenClarification]["DURATION"]) > 0)
				{
					Storage::put('duration', $arQuestion["ANSWERS"][$intChosenClarification]["DURATION"]);
				}

				if(intval($arQuestion["ANSWERS"][$intChosenClarification]["COST"]) > 0)
				{
					Storage::put('cost', $arQuestion["ANSWERS"][$intChosenClarification]["COST"]);
				}
			}
		}

		return true;
	}

	function get ($id)
	{
		$arAjaxResult = Service::getClarifications($id);

		foreach ($arAjaxResult as &$arr)
		{
			foreach ($arr['ANSWERS'] as &$arAnswer)
			{
				$arAnswer = array_only($arAnswer, ["ID", "NAME", "CLARIFICATION", "DURATION"]);
			}
			$arr['ANSWERS'] = array_assoc($arr['ANSWERS'], "ID");
			$arr['INPUT_NAME'] = 'question_' . $arr["ID"];
		}

		return $arAjaxResult;
	}

	function all ()
	{
		$arServices = Service::all();

		foreach ($arServices as &$arService)
		{
			$arService = array_only($arService, ["NAME", "ID", "DETAIL_LINK", "DURATION", "COST"]);
		}

		return $arServices;
	}
}
