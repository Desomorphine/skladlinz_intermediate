<?php

use Focus\Component;
use Focus\Repository\Doctor;
use Focus\Repository\Salon;
use Focus\Storage;
use Focus\Repository\Answers;
use Itgro\App;
use Itgro\Request\Request;


class ServiceComponent extends Component
{
	function save()
	{
		$salonId = Request::put()->get('salon');

		if ($salonId > 0)
		{
			Storage::put('salon', $salonId);
			return true;
		}
		else
		{
			$this->save_error = 'Не указан ID салона';
			return false;
		}
	}

	function get ($id)
	{
		$this->all ();
	}

	function all ()
	{
		$arResult = array(
			"IS_HELD" => "Y"
		);

		$arFilter = array
		(
			"DOCTOR_TYPE" => array_pluck(Doctor::all(), 'ID'),
		);

		$arClarifications = Answers::GetList(["=ID" => Storage::get('clarifications')]);

		foreach ($arClarifications as $arClarification)
		{
			if(no($arClarification["IS_HELD"]))
			{
				$arResult["IS_HELD"] = "N";
				break;
			}

			$arFilter["DOCTOR_TYPE"] = array_intersect($arFilter["DOCTOR_TYPE"], $arClarification["DOCTOR_TYPE"]);

			$arFilter["LICENSE"] = yes($arClarification["LICENSE"]) ? 'YES': 'NO';
		}

		if($arResult["IS_HELD"] == "Y")
		{
			if(array_key_exists("LICENSE", $arFilter))
			{
				if(yes($arFilter["LICENSE"]))
				{
					$arFilter["PROPERTY_LICENSE_VALUE"] = "Да";
				}
				else
				{
					$arFilter["!PROPERTY_LICENSE_VALUE"] = "Да";
				}

				unset($arFilter["LICENSE"]);
			}

			foreach($arFilter["DOCTOR_TYPE"] as $doctorID)
			{
				// TODO Костыль вынести в модель!
				$arFilter[] = ["ID" => CIBlockElement::SubQuery("ID", ["IBLOCK_ID" => Salon::GetIblockID(), "PROPERTY_DOCTORS" => $doctorID])];
			}

			unset($arFilter["DOCTOR_TYPE"]);

			$salons = Salon::getList($arFilter);
			$arResult = [];

			foreach ($salons as $key => $value)
			{
				$arResult["SALONS"][$key] = array_only($value, ["ID", "ADDRESS", "DETAIL_TEXT", "PHONE", "WORKTIME", "TRAMS", "BUSES", "CABS"]);
			}
		}
		else
		{
			$arResult["SALONS"] = [];
		}

		return $arResult;
	}
}
