<?
require_once(dirname(__FILE__)."/../prolog.php");
require_once(dirname(__FILE__)."/../lib/util.php");

use Bitrix\Main\Localization\Loc;
use Itgro\Util;

Loc::loadMessages(__FILE__);

Class entry extends CModule
{
	var $MODULE_ID = 'entry';
	var $MODULE_SORT;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $PARTNER_NAME;
	var $PARTNER_URI;
	var $MODULE_DIR;

	public function __construct()
	{
		/** @var array $arModuleVersion */

		require(dirname(__FILE__)."/version.php");

		$this->MODULE_SORT = 10000;
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("ENTRY_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ENTRY_MODULE_DESC");
		$this->PARTNER_NAME = GetMessage("ENTRY_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("ENTRY_PARTNER_URI");
		$this->MODULE_DIR = dirname(__FILE__)."/../";

		$this->module_code = 'entry';
		$this->clients_iblock_id = 0;
	}

	private function InstallIBlocks()
	{
		$arErrors = array();

		$obIBlockType = new CIBlockType();

		$dbIBlockType = $obIBlockType->GetByID($this->module_code);

		$arIBlockType = $dbIBlockType->GetNext(true, false);

		//TODO setOptionInt на все инфоблоки

		if (!Util::IsNotEmptyArray($arIBlockType))
		{
			$obIBlockType->Add(
				array(
					"ID" => $this->MODULE_NAME,
					"SECTIONS" => "N",
					"IN_RSS" => "N",
					"SORT" => 1000,
					"LANG" => array(
						"ru" => array(
							"NAME" => "Интерфейс техподдержки",
							"SECTION_NAME" => "Раздел",
							"ELEMENT_NAME" => "Клиент"
						)
					)
				)
			);
		}

		$obIBlock = new CIBlock();

		$dbIBlock = $obIBlock->GetList(array("ID" => "ASC"), array("CODE" => "clients", "TYPE" => $this->MODULE_NAME));

		if ($arClientsIBlock = $dbIBlock->GetNext(true, false))
		{
			if ($arClientsIBlock["ACTIVE"] === "N")
			{
				$obIBlock->Update($arClientsIBlock["ID"], array("ACTIVE" => "Y"));
			}
		}
		else
		{
			$arClientsIBlock = array(
				"ACTIVE" => "Y",
				"NAME" => "Клиенты",
				"CODE" => "clients",
				"IBLOCK_TYPE_ID" => $this->MODULE_NAME,
				"SITE_ID" => array(self::getExtranetSiteID()),
				"SORT" => 10,
				"VERSION" => 2,
				"WORKFLOW" => "N",
				"INDEX_ELEMENT" => "N",
			);

			if ($iblockID = $obIBlock->Add($arClientsIBlock))
			{
				$arClientsIBlock["ID"] = $iblockID;
			}
		}

		if (Util::IsIntegerAboveZero($arClientsIBlock["ID"]))
		{
			$this->clients_iblock_id = intval($arClientsIBlock["ID"]);

			COption::SetOptionInt($this->MODULE_NAME, "clients_iblock_id", $this->clients_iblock_id);

			$obIBlockProperty = new CIBlockProperty();

			$dbIBlockPropertyCategory = $obIBlockProperty->GetList(
				array("id" => "asc"),
				array("CODE" => "CATEGORY", "IBLOCK_ID" => $arClientsIBlock["ID"])
			);

			if ($arIBlockPropertyCategory = $dbIBlockPropertyCategory->GetNext(true, false))
			{
				if ($arIBlockPropertyCategory["ACTIVE"] === "N")
				{
					$obIBlockProperty->Update($arIBlockPropertyCategory["ID"], array("ACTIVE" => "Y"));
				}
			}
			else
			{
				$arIBlockPropertyCategory = Array(
					"NAME" => "Категория",
					"ACTIVE" => "Y",
					"SORT" => "100",
					"CODE" => "CATEGORY",
					"PROPERTY_TYPE" => "L",
					"LIST_TYPE" => "L",
					"IBLOCK_ID" => $arClientsIBlock["ID"],
					"MULTIPLE" => "Y",
					"IS_REQUIRED" => "Y",
					"USER_TYPE" => "itgro_support_dictionary_category",
				);

				if ($propertyCategoryID = $obIBlockProperty->Add($arIBlockPropertyCategory))
				{
					$arIBlockPropertyCategory["ID"] = $propertyCategoryID;
				}
			}

			if (Util::IsIntegerAboveZero($arIBlockPropertyCategory["ID"]))
			{
				COption::SetOptionInt($this->MODULE_NAME, "clients_property_category_id", intval($arIBlockPropertyCategory["ID"]));
			}
			else
			{
				$arErrors[] = $obIBlockProperty->LAST_ERROR;
			}
		}
		else
		{
			$arErrors[] = $obIBlock->LAST_ERROR;
		}

		return $arErrors;
	}

	private function InstallUserFields()
	{
		$arErrors = array();

		$obUserField = new CUserTypeEntity;

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "USER", "FIELD_NAME" => "UF_SUP_CATEGORY")
		);

		if (!$arSupportCategory = $dbUserField->GetNext(true, false))
		{
			$arSupportCategory = array(
				"ENTITY_ID" => "USER",
				"FIELD_NAME" => "UF_SUP_CATEGORY",
				"USER_TYPE_ID" => "itgro_support_dictionary_category",
				"XML_ID" => "",
				"SORT" => 100,
				"MULTIPLE" => "Y",
				"EDIT_FORM_LABEL" => array(
					"ru" => "Категория сотрудника техподдержки",
					"en" => "Support employee category",
				),
				"LIST_COLUMN_LABEL" => array(
					"ru" => "Категория сотрудника техподдержки",
					"en" => "Support employee category",
				),
				"LIST_FILTER_LABEL" => array(
					"ru" => "Категория сотрудника техподдержки",
					"en" => "Support employee category",
				),
				"ERROR_MESSAGE" => array(
					"ru" => "",
					"en" => "",
				),
				"HELP_MESSAGE" => array(
					"ru" => "",
					"en" => "",
				),
			);

			if ($supportCategoryID = $obUserField->Add($arSupportCategory))
			{
				$arSupportCategory["ID"] = $supportCategoryID;
			}
		}

		if (!Util::IsIntegerAboveZero($arSupportCategory["ID"]))
		{
			$arErrors[] = "Не удалось создать свойство \"Категория сотрудника техподдержки\"";
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "USER", "FIELD_NAME" => "UF_CRM_CLIENT")
		);

		if (!$arSupportClient = $dbUserField->GetNext(true, false))
		{
			$arSupportClient = array(
				"ENTITY_ID" => "USER",
				"FIELD_NAME" => "UF_CRM_CLIENT",
				"USER_TYPE_ID" => "iblock_element",
				"XML_ID" => "",
				"SORT" => 100,
				"MULTIPLE" => "Y",
				"SETTINGS" => array(
					"IBLOCK_TYPE_ID" => $this->MODULE_NAME,
					"IBLOCK_ID" => $this->clients_iblock_id,
					"DISPLAY" => "LIST",
					"LIST_HEIGHT" => 5,
					"ACTIVE_FILTER" => "Y",
				),
				"EDIT_FORM_LABEL" => array(
					"ru" => "Клиент техподдержки",
					"en" => "Support client",
				),
				"LIST_COLUMN_LABEL" => array(
					"ru" => "Клиент техподдержки",
					"en" => "Support client",
				),
				"LIST_FILTER_LABEL" => array(
					"ru" => "Клиент техподдержки",
					"en" => "Support client",
				),
				"ERROR_MESSAGE" => array(
					"ru" => "",
					"en" => "",
				),
				"HELP_MESSAGE" => array(
					"ru" => "",
					"en" => "",
				),
			);

			if ($supportClientID = $obUserField->Add($arSupportClient))
			{
				$arSupportClient["ID"] = $supportClientID;
			}
		}

		if (!Util::IsIntegerAboveZero($arSupportClient["ID"]))
		{
			$arErrors[] = "Не удалось создать свойство \"Клиент техподдержки\"";
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "SUPPORT", "FIELD_NAME" => "UF_STATUS_CH_DATE")
		);

		if (!$arSupportField = $dbUserField->GetNext(true, false))
		{
			$arSupportField = array(
				"USER_TYPE_ID" => "datetime",
				"ENTITY_ID" => "SUPPORT",
				"FIELD_NAME" => "UF_STATUS_CH_DATE",
				"XML_ID" => "XML_STATUS_CH_DATE",
				"EDIT_FORM_LABEL" => array(
					"ru" => 'Дата последнего изменения статуса',
					"en" => 'Date of last status change'
				)
			);

			if ($supportFieldID = $obUserField->Add($arSupportField))
			{
				$arSupportField["ID"] = $supportFieldID;
			}
		}

		if (!Util::IsIntegerAboveZero($arSupportField["ID"]))
		{
			$arErrors[] = "Не удалось создать свойство \"Дата последнего изменения статуса\"";
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "SUPPORT", "FIELD_NAME" => "UF_AUTO_CLOSE")
		);

		if (!$arSupportField = $dbUserField->GetNext(true, false))
		{
			$arSupportField = array(
				"USER_TYPE_ID" => "date",
				"ENTITY_ID" => "SUPPORT",
				"FIELD_NAME" => "UF_AUTO_CLOSE",
				"XML_ID" => "XML_AUTO_CLOSE",
				"EDIT_FORM_LABEL" => array(
					"ru" => 'Статус(дата) автозакрытия заявки',
					"en" => 'Ticket auto close status(date)'
				)
			);

			if ($supportFieldID = $obUserField->Add($arSupportField))
			{
				$arSupportField["ID"] = $supportFieldID;
			}
		}

		if (!Util::IsIntegerAboveZero($arSupportField["ID"]))
		{
			$arErrors[] = "Не удалось создать свойство \"Статус автозакрытия заявки\"";
		}

		return $arErrors;
	}

	public function InstallMailTemplates()
	{
		$arErrors = array();
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$rsSites = CSite::GetList($by = "sort", $order = "desc");
		$sites = array();
		while ($arSites = $rsSites->GetNext())
		{
			$sites[] = $arSites['ID'];
		}

		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		if (CEventType::GetByID("SUP_SEND_MAIL", "ru")->Fetch() == false)
		{
			//Если не существует, то создадим
			$obEventType = new CEventType;

			$obEventType->Add(array(
				"EVENT_NAME" => "SUP_SEND_MAIL",
				"NAME" => "ТП: оповещение пользователя на email",
				"SITE_ID" => "ru",
				"DESCRIPTION" => "
					#MAIL_TO# - Адрес на который придёт сообщение
					#MAIL_FROM# - Адрес от которого придёт сообщение
					#TICKET_ID# - ID обращения
					#TICKET_TITLE# - Заголовок обращения
					#TICKET_CAT# - Категория обращения
					#TICKET_WARR# - Гарантия
					#AUTO_CLOSE_DATE# - Дата автозакрытия
					#TICKET_STATUS# - Статус обращения
					#MESSAGE# - Сообщение
					#NAME# - Имя получателя
					#LAST_NAME# - Фамилия получателя
					#SECOND_NAME# - Отчество получателя
					#RESPONSIBLE_NAME# - Имя Фамилия ответственного
                "
			));


			$arrStatusCh["ACTIVE"] = "Y";
			$arrStatusCh["EVENT_NAME"] = "SUP_SEND_MAIL";
			$arrStatusCh["LID"] = $sites;
			$arrStatusCh["EMAIL_FROM"] = "#MAIL_FROM#";
			$arrStatusCh["EMAIL_TO"] = "#MAIL_TO#";
			$arrStatusCh["BCC"] = "";
			$arrStatusCh["SUBJECT"] = "Изменен статус обращения #TICKET_ID#(#TICKET_TITLE#)";
			$arrStatusCh["BODY_TYPE"] = "html";
			$arrStatusCh["MESSAGE"] = "
			<html>
			<head>
				<meta content='text/html' charset='utf-8'>
			</head>
			<body>
				<h2>
					<a href='#TICKET_URL#'>##TICKET_ID#: #TICKET_TITLE#</a>
				</h2>
				#NAME# #SECOND_NAME#, внимание! изменился статус у обращения <a href='#TICKET_URL#'>##TICKET_ID#(#TICKET_TITLE#)</a>.<br>
				Статус обращения - #TICKET_STATUS#<br>
				Ответственный - #RESPONSIBLE_NAME#<br>
				Гарантия - #TICKET_WARR#<br><br>

				#MESSAGE#
			</body>
			</html>

";
			$obTemplate = new CEventMessage;
			if (!Util::IsIntegerAboveZero($statusChID = $obTemplate->Add($arrStatusCh)))
			{
				$arErrors[] = "Не удалось создать шаблон \"Изменен статус обращения\"";
			}
			else
			{
				//Записать айдишник
				COption::SetOptionInt("itgro.support", "email.statusChangeTemplate", $statusChID);
			}

			$arrStatusCh["ACTIVE"] = "Y";
			$arrStatusCh["EVENT_NAME"] = "SUP_SEND_MAIL";
			$arrStatusCh["LID"] = $sites;
			$arrStatusCh["EMAIL_FROM"] = "#MAIL_FROM#";
			$arrStatusCh["EMAIL_TO"] = "#MAIL_TO#";
			$arrStatusCh["BCC"] = "";
			$arrStatusCh["SUBJECT"] = "Новое сообщение в обращении ##TICKET_ID#(#TICKET_TITLE#)";
			$arrStatusCh["BODY_TYPE"] = "html";
			$arrStatusCh["MESSAGE"] = "
			<html>
			<head>
				<meta content='text/html' charset='utf-8'>
			</head>
			<body>
				<h2>
					<a href='#TICKET_URL#'>##TICKET_ID#: #TICKET_TITLE#</a>
				</h2>
				#NAME# #SECOND_NAME#, внимание! В обращении <a href='#TICKET_URL#'>##TICKET_ID#(#TICKET_TITLE#)</a> добавлено новое сообщение.<br>
				Статус обращения - #TICKET_STATUS#<br>
				Ответственный - #RESPONSIBLE_NAME#<br>
				Гарантия - #TICKET_WARR#<br><br>

				#MESSAGE#
			</body>
			</html>

";
			$obTemplate = new CEventMessage;
			if (!Util::IsIntegerAboveZero($statusChID = $obTemplate->Add($arrStatusCh)))
			{
				$arErrors[] = "Не удалось создать шаблон \"Новое сообщение в обращении\"";
			}
			else
			{
				//Записать айдишник
				COption::SetOptionInt("itgro.support", "email.newMessageTemplate", $statusChID);
			}

		}

		return $arErrors;

	}

	public function InstallFiles($arParams = array())
	{
		$root = \Bitrix\Main\Application::getInstance()->getDocumentRoot();

		if (stripos(__FILE__, $root . "/local") === 0)
		{
			$bx_dir = "local";
		}
		else
		{
			$bx_dir = "bitrix";
		}

		$componentsDir = $this->MODULE_DIR."/install/components";

		if (is_dir($componentsDir))
		{
			if ($dir = opendir($componentsDir))
			{
				while (false !== $namespace = readdir($dir))
				{
					if ($namespace == ".." || $namespace == ".")
					{
						continue;
					}

					CopyDirFiles($componentsDir."/".$namespace, $root."/" . $bx_dir . "/components/".$namespace, true, true);
				}

				closedir($dir);
			}
		}

		$templatesDir = $this->MODULE_DIR."/install/templates";

		if (is_dir($templatesDir))
		{
			if ($dir = opendir($templatesDir))
			{
				while (false !== $templateName = readdir($dir))
				{
					if ($templateName == ".." || $templateName == ".")
					{
						continue;
					}

					CopyDirFiles($templatesDir."/".$templateName, $root."/" . $bx_dir . "/templates/".$templateName, true, true);
				}

				closedir($dir);
			}
		}

		$publicDir = $this->MODULE_DIR."/install/public";

		if (is_dir($publicDir))
		{
			if ($dir = opendir($publicDir))
			{
				while (false !== $folderName = readdir($dir))
				{
					if ($folderName == ".." || $folderName == ".")
					{
						continue;
					}

					CopyDirFiles($publicDir."/".$folderName, $root.self::getExtranetSiteDir().$folderName, true, true);
				}

				closedir($dir);
			}
		}

		$URLRewriter = new CUrlRewriter();

		$URLRewriter->Add(
			array(
				"SITE_ID" => "co",
				"CONDITION" => "#^".self::getExtranetSiteDir()."".$this->MODULE_NAME."/#",
				"ID" => "itgro:support",
				"PATH" => self::getExtranetSiteDir()."".$this->MODULE_NAME."/index.php",
				"RULE" => "",
			)
		);

		return true;
	}

	public function UnInstallFiles()
	{
		$root = \Bitrix\Main\Application::getInstance()->getDocumentRoot();

		if (stripos(__FILE__, $root . "/local") === 0)
		{
			$bx_dir = "local";
		}
		else
		{
			$bx_dir = "bitrix";
		}

		$componentsDir = $this->MODULE_DIR."/install/components";

		if (is_dir($componentsDir))
		{
			if ($dir = opendir($componentsDir))
			{
				while (false !== $namespace = readdir($dir))
				{
					if ($namespace == ".." || $namespace == ".")
					{
						continue;
					}

					$namespaceDirName = $componentsDir."/".$namespace;

					if (is_dir($namespaceDirName))
					{
						$namespaceDir = opendir($namespaceDirName);

						while (false !== $componentDir = readdir($namespaceDir))
						{
							if ($componentDir == ".." || $componentDir == ".")
							{
								continue;
							}

							DeleteDirFilesEx("/" . $bx_dir . "/components/".$namespace."/".$componentDir);
						}

						closedir($namespaceDir);
					}
				}

				closedir($dir);
			}
		}

		$templatesDir = $this->MODULE_DIR."/install/templates";

		if (is_dir($templatesDir))
		{
			if ($dir = opendir($templatesDir))
			{
				while (false !== $templateName = readdir($dir))
				{
					if ($templateName == ".." || $templateName == ".")
					{
						continue;
					}

					DeleteDirFilesEx("/" . $bx_dir . "/templates/".$templateName);
				}

				closedir($dir);
			}
		}

		$publicDir = $this->MODULE_DIR."/install/public";

		if (is_dir($publicDir))
		{
			if ($dir = opendir($publicDir))
			{
				while (false !== $folderName = readdir($dir))
				{
					if ($folderName == ".." || $folderName == ".")
					{
						continue;
					}

					DeleteDirFilesEx(self::getExtranetSiteDir().$folderName);
				}

				closedir($dir);
			}
		}

		$URLRewriter = new CUrlRewriter();

		$URLRewriter->Delete(array("SITE_ID" => "co", "CONDITION" => "#^/extranet/".$this->MODULE_NAME."/#"));

		return true;
	}

	public function InstallDB()
	{
		$arTemplates = self::getExtranetSiteTemplates();

		$arTemplates[] = array(
			"CONDITION" => "CSite::InDir('".self::getExtranetSiteDir().$this->MODULE_NAME."/')",
			"SORT" => 1,
			"TEMPLATE" => $this->MODULE_NAME
		);

		$obSite = new CSite();

		$obSite->Update(self::getExtranetSiteID(), array("TEMPLATE" => $arTemplates));

		RegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "itgro.support", "Itgro\UserField\SupportDictionaryCategory", "GetUserTypeDescription");
		RegisterModuleDependences("main", "OnUserTypeBuildList", "itgro.support", "Itgro\UserField\SupportDictionaryCategory", "GetUserTypeDescription");

		RegisterModuleDependences("support", "OnBeforeTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "stashPreviousStatusID");

		RegisterModuleDependences("support", "OnAfterTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "prepareNotifications");
		RegisterModuleDependences("main", "OnEpilog", "itgro.support", "\Itgro\Support\EventHandlers", "sendNotifications");

		RegisterModuleDependences("support", "OnAfterTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "logStatusChange");

		$arIblockErrors = $this->InstallIBlocks();

		$arUFErrors = $this->InstallUserFields();

		$arMailErrors = $this->InstallMailTemplates();

		$arErrors = array_merge($arIblockErrors, $arUFErrors, $arMailErrors);

		// TODO (itgro): Обработать ошибки
		if (Util::IsNotEmptyArray($arErrors))
		{
			Util::Dump($arErrors);
			die();
		}
	}

	public function UninstallDB()
	{
		$arNewTemplates = array();

		$arTemplates = self::getExtranetSiteTemplates();

		if (is_array($arTemplates) && !empty($arTemplates))
		{
			foreach ($arTemplates as $arTemplate)
			{
				if (
					$arTemplate["CONDITION"] != "CSite::InDir('".self::getExtranetSiteDir().$this->MODULE_NAME."/')" &&
					$arTemplate["TEMPLATE"] != $this->MODULE_NAME
				)
				{
					$arNewTemplates[] = $arTemplate;
				}
			}
		}

		$obSite = new CSite();

		$obSite->Update(self::getExtranetSiteID(), array("TEMPLATE" => $arNewTemplates));

		$obIBlockType = new CIBlockType();

		$dbIBlockType = $obIBlockType->GetByID($this->MODULE_NAME);

		$arIBlockType = $dbIBlockType->GetNext(true, false);

		if (Util::IsNotEmptyArray($arIBlockType))
		{
			$obIBlockType->Delete($this->MODULE_NAME);
		}

		$obUserField = new CUserTypeEntity;

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "USER", "FIELD_NAME" => "UF_I_SUPPORT_CATEGORY")
		);

		if ($arSupportCategory = $dbUserField->GetNext(true, false))
		{
			$obUserField->Delete($arSupportCategory["ID"]);
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "USER", "FIELD_NAME" => "UF_I_SUPPORT_CLIENT")
		);

		if ($arSupportClient = $dbUserField->GetNext(true, false))
		{
			$obUserField->Delete($arSupportClient["ID"]);
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "SUPPORT", "FIELD_NAME" => "UF_STATUS_CH_DATE")
		);

		if ($arSupportClient = $dbUserField->GetNext(true, false))
		{
			$obUserField->Delete($arSupportClient["ID"]);
		}

		$dbUserField = $obUserField->GetList(
			array("ID" => "ASC"),
			array("ENTITY_ID" => "SUPPORT", "FIELD_NAME" => "UF_AUTO_CLOSE")
		);

		if ($arSupportClient = $dbUserField->GetNext(true, false))
		{
			$obUserField->Delete($arSupportClient["ID"]);
		}

		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$dbEventMessages = CEventMessage::GetList($by = "id", $order = "desc", array(
			"TYPE_ID" => "SUP_SEND_MAIL || BLAH_BLAH_BLAH"
		));

		while ($arEventMessage = $dbEventMessages->Fetch())
		{
			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			CEventMessage::Delete($arEventMessage['ID']);
		}

		UnRegisterModuleDependences("iblock", "OnIBlockPropertyBuildList", "itgro.support", "Itgro\UserField\SupportDictionaryCategory", "GetUserTypeDescription");
		UnRegisterModuleDependences("main", "OnUserTypeBuildList", "itgro.support", "Itgro\UserField\SupportDictionaryCategory", "GetUserTypeDescription");

		UnRegisterModuleDependences("support", "OnBeforeTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "stashPreviousStatusID");

		UnRegisterModuleDependences("support", "OnAfterTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "prepareNotifications");
		UnRegisterModuleDependences("main", "OnEpilog", "itgro.support", "\Itgro\Support\EventHandlers", "sendNotifications");

		UnRegisterModuleDependences("support", "OnAfterTicketUpdate", "itgro.support", "\Itgro\Support\EventHandlers", "logStatusChange");

		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		if (CEventType::GetByID("SUP_SEND_MAIL", "ru")->Fetch() != false)
		{
			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			CEventType::Delete("SUP_SEND_MAIL");
		}
	}

	public function DoInstall()
	{
		RegisterModule($this->MODULE_ID);

//		$this->InstallFiles();

		//$this->InstallDB();
	}

	public function DoUninstall()
	{
//		$this->UnInstallDB();

		//$this->UnInstallFiles();

		UnRegisterModule($this->MODULE_ID);
	}
}
