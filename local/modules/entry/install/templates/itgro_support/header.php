<? (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

/* @global CMain $APPLICATION */
/* @var CBitrixComponent $component */
/* @var array $arParams */
/* @var array $arResult */
?>

<? $assetsPath = "/local/templates/itgro_support/assets"; ?>

<!doctype html>
<html lang="ru" class="no-js">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?= $assetsPath; ?>/css/main.css">
	<link rel="icon" href="/favicon.png">
	<!--[if IE]>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <![endif]-->
	<!--[if IE 9]>
		<link rel="stylesheet" href="<?= $assetsPath; ?>/css/ie9.css">
	<![endif]-->
	<!--[if IE 8]>
		<link rel="stylesheet" href="<?= $assetsPath; ?>/css/ie8.css">
		<script src="<?= $assetsPath; ?>/js/lib/html5shiv.js"></script>
		<script src="<?= $assetsPath; ?>/js/lib/selectivizr-min.js"></script>
	<![endif]-->
	<title><? $APPLICATION->ShowTitle(); ?></title>
	<? $APPLICATION->ShowHead(); ?>
</head>
<body class="body">
	<div class="page-wrapper">
		<noscript>
			<p class="browser-error">Для полной функциональности этого сайта необходимо включить JavaScript.<br>
				<a href="http://www.enable-javascript.com/ru/" target="_blank">Инструкции, как включить JavaScript в вашем браузере</a>
			</p>
		</noscript>

		<!--[if lte IE 7]>
			<p class="browser-error">Вы используете устаревший браузер. Пожалуйста, <a href="http://browsehappy.com/" target="_blank">обновите свой браузер</a></p>
		<![endif]-->

		<?

		$APPLICATION->IncludeComponent(
		    "itgro:support.header",
		    ".default",
		    array(
				"LOGO_PATH" => SITE_TEMPLATE_PATH."/assets/img/logo.png",
			    "LOGO_HEIGHT" => 40,
			    "LOGO_WIDTH" => 200,
		    )
		);
		?>

		<div class="wrapper">
			<?
			$APPLICATION->IncludeComponent(
				"itgro:support.breadcrumbs",
				".default",
				array(
					"START_FROM" => 1,
				),
				false
			);
			?>