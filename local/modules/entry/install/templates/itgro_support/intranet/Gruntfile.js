module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			dev: {
				files: [{
					expand: true,
					cwd: 'assets/src/js',
					src: '**/*.js',
					dest: 'assets/js/',
					ext: '.min.js',
				}]
			},
		},

		watch: {
			css: {
				files: 'assets/src/scss/**',
				tasks: 'compass'
			},
			js: {
				files: 'assets/src/js/**',
				tasks: 'uglify'
			}
		},

		imagemin: {
			png: {
				options: {
					optimizationLevel: 7
				},
				files: [
					{
						expand: true,
						cwd: 'assets/img/',
						src: ['**/*.png'],
						dest: 'assets/img/',
						ext: '.png'
					}
				]
			},
			jpg: {
				options: {
					progressive: true
				},
				files: [
					{
						expand: true,
						cwd: 'assets/img/',
						src: ['**/*.jpg'],
						dest: 'assets/img/',
						ext: '.jpg'
					}
				]
			}
		},

		compass: {
			dev: {
				options: {
					sassDir: 'assets/src/scss/',
					cssDir: 'assets/css/',
					outputStyle: 'compressed',
					imageDir: 'assets/img',
					imagesPath: 'assets/src/img',
					generatedImagesDir: 'assets/img',
					generatedImagesPath: 'assets/img',
				}
			}
		},

		copy: {
			main: {
				files: [
					{expand: true, src: 'assets/src/js', dest: 'assets/js', filter: 'isFile'}
				]
			},
		},

		clean: {
			clean: ["assets/css", "assets/js", "assets/img"]
		}

	});

	grunt.registerTask('default', ['uglify', 'compass', 'copy', 'watch']);
	grunt.registerTask('css', ['compass', 'copy', 'watch:css']);
	grunt.registerTask('js', ['uglify', 'watch:js']);
	grunt.registerTask('build', ['clean', 'uglify', 'compass', 'copy', 'imagemin']);
};