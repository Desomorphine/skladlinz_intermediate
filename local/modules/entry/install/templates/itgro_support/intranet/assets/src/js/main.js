$(document).ready( function() {
	$('html').removeClass('no-js');
	//Размеры видимой части экрана
	function getViewportSize() {
		var a = document.documentElement,
			d = document.body,
			b = document.compatMode == "CSS1Compat",
			c = window.opera,
			e = b && c ? window.innerWidth : b && !c ? a.clientWidth : d.clientWidth;
			a = b && c ? window.innerHeight : b && !c ? a.clientHeight : d.clientHeight;
		return [e, a] //ширина и высота видимой части
	};

	//Ширина скролла
	function getScrollWidth() {
		var div = document.createElement('div'),
			scrollWidth;

		div.style.overflowY = 'scroll';
		div.style.width =  '50px';
		div.style.visibility = 'hidden';

		document.body.appendChild(div);
		scrollWidth = div.offsetWidth - div.clientWidth;
		document.body.removeChild(div);

		return scrollWidth;
	}

	$('.js-toggle').on(
		'click',
		function() {
			$('.js-toggle-item').slideToggle(200);
		})
	$('.js-collapse').on(
		'click',
		function() {
			$($(this).data('toggle-item')).slideToggle(200);
			$(this).toggleClass('show');
		});

	//Показать все системные сообщения
	$('.js-show-system-message').on(
		'click',
		function() {
			var el = $(this);

			el.toggleClass('show');
			if (el.hasClass('show')) {
				el.text('Скрыть системные уведомления');
				$('.reply--system').find('.js-collapse').each(function() {
					$(this).hasClass('show') ? '' : $(this).click();
				})
			} else {
				el.text('Показать системные уведомления');
				$('.reply--system').find('.js-collapse').each(function() {
					$(this).hasClass('show') ? $(this).click() : '' ;
				})
			}
		})

	//Таймер
	var intervalID;
	function getZero(el) {
		el = el < 10 ? '0'+el : el;
		return el;
	}
	function timer(el) {
		var s = +parseInt(el.find('.timer__time-s').text(), 10) + 1,
			m = +parseInt(el.find('.timer__time-m').text(), 10),
			h = +parseInt(el.find('.timer__time-h').text(), 10);

			if (s > 59) {
				s = 0;
				m +=1;
				if (m > 59) {
					m = 0;
					h +=1;
				}
			}

			el.find('.timer__time-s').text(getZero(s));
			el.find('.timer__time-m').text(getZero(m)+' : ');
			el.find('.timer__time-h').text(getZero(h)+' : ');
	}
	$('.js-timer').on(
		'click',
		function() {
			timerToggle($(this), "N");
		});

	if($('.js-timer').data('on_page_init') == "Y")
	{
		timerToggle($('.js-timer'), "Y");
	}

	function timerToggle(timerObj, timerInit)
	{
		var el = timerObj,
				t,
				action;

		if(timerInit == "N")
		{
			var params,
				ticketAction;

			if (el.hasClass('timer__action--pause'))
				action = 'PAUSE';
			else
				action = 'START';

			if(timerObj.data("action") && timerObj.data("action_value"))
			{
				ticketAction = timerObj.data("action");

				params = {};

				params[ticketAction] = timerObj.data("action_value");
				params['ACTION_TIMER'] = action;
				params['AJAX'] = 'Y';

				timerObj.removeAttr("data-action");
				timerObj.removeAttr("data-action_value");
			}

			$.ajax({
			   type: "POST",
			   url: document.location.href,
			   data: params,
			   success: function(res){
			    res = JSON.parse(res);
				if (res['CODE'] == 'SUCCESS')
				{
					el.toggleClass('timer__action--pause');

					if (action == 'START') {
						el.text('Приостановить');
						intervalID = setInterval(
							function() {
								t = el.closest('.timer');
								timer(t);
							}, 1000);
					} else {
						el.text('Начать выполнение');
						clearTimeout(intervalID);
					}
				}
			   }
			});
		}
		else if (timerInit == "Y")
		{
			el.toggleClass('timer__action--pause');
			el.text('Приостановить');
			intervalID = setInterval(
				function() {
					t = el.closest('.timer');
					timer(t);
				}, 1000);
		}
	}

	//Модальные окна
	var modal = {
		getShow: function(el, deleteModal) {
			if (el.length) {
				var overlay = el.closest('.overlay').addClass('active');

				if (el.closest('.modal').outerHeight() < getViewportSize()[1]) {
					overlay.css({
						height: 100 + '%',
					})
				}
				$('body').addClass('modal-show').css({
					paddingRight: getScrollWidth(),
					marginRight: - getScrollWidth(),
					marginLeft: - getScrollWidth(),
				});

				deleteModal ? this.close($('.js-modal-close'), true) : this.close($('.js-modal-close'));
			}
		},
		open: function() {
			var self = this;

			$('.js-modal-open').on(
				'click',
				function() {
					self.getShow($($(this).data('modal')));
				})
		},
		close: function(el, deleteModal) {
			el.on(
				'click',
				function() {
					$(this).closest('.overlay').removeClass('active');

					if (!$('.overlay.active').length) {
						$('body').removeClass('modal-show').css({
							paddingRight: 0,
							marginRight: 0,
							marginLeft: 0,
						});
					}

					deleteModal ? $(this).closest('.overlay').remove() : '';
				})
		},
		systemMessage: function(obj) {
			if (!$('.modal-wrap').length) {
				$('body').append('<div class="modal-wrap"></div>');
			};

			var modalSystem = $('.modal-wrap').append(
				'<div class="overlay js-modal-close">' +
					'<div class="modal modal--min">' +
						'<div class="modal__close js-modal-close">&#215;</div>' +
						'<div class="modal__title">' + obj.title + '</div>' + obj.message +
					'</div>' +
				'</div>').find('.overlay:last');

			this.getShow(modalSystem, true);
		},
	}

	modal.open();
	$('.modal').on(
		'click',
		function(e) {
			e.stopPropagation();
		});

	//Редактирование времени
	var timeEdit = {
		toggle: function() {
			this.helper.find('.js-toggle-hidden').toggleClass('hidden');
		},
		showForm: function() {
			var self = this;

			$('.js-change-time').on(
				'click',
				function() {
					self.helper = $(this).closest('.reply__helper');
					self.toggle();
				});
		},
		getHour: function() {
			var self = this,
				t = 0;

			$('.js-change-time-form .form-control').on(
				'keyup',
				function() {
					clearTimeout(t);
					t = setTimeout(function() {
						var hourVal = self.helper.find('.js-change-time-form__hour').val(),
							minVal = self.helper.find('.js-change-time-form__min').val(),
							hour = parseInt((hourVal == '') ? 0 : hourVal),
							min = Math.ceil((minVal == '') ? 0 : minVal),
						 	i = 0;

						function getTime() {
							if (min >= 60) {
								i++;
								min -= 60;
								getTime();
							}
						};
						getTime();
						hour += i

						self.hour = hour;
						self.min = min;
						self.helper.find('.js-change-time-form__hour').val(hour);
						self.helper.find('.js-change-time-form__min').val(min);
					}, 500)
				});
		},
		replaceTime: function() {
			var self = this;

			$('.js-change-time-form').each(function() {
				$(this).on(
					'submit',
					function() {
						if ('min' in self || 'hour' in self) {
							self.helper.find('.reply__helper-time-current').text(self.hour + ':' + self.min);
						}
						self.toggle();

						return true;
					});
			})
		}
	}

	timeEdit.showForm();
	timeEdit.getHour();
	timeEdit.replaceTime();

	//Переделать на AJAX отправку обращения!
	$('.js-send').on(
		'click',
		function() {
			modal.systemMessage({
				title: 'Обращение отправлено в работу',
				message: '<div class="btn-group btn-group--double"><a href="#0" class="btn">К обращению</a> <a href="#0" class="btn">К списку обращений</a></div>',
			});
		});

	//Select
	$('select.form-control').chosen({
		disable_search_threshold: 10,
		no_results_text: "Не найдено совпадений",
	});
})