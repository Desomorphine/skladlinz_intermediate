<? /** @var string $assetsPath */ ?>
		</div>

		<div class="footer-buffer"></div>

	</div>

	<div class="footer-line">
		<footer class="footer">
			&copy; 2001-2012 &#171;Битрикс&#187;, &#171;1С-Битрикс&#187;. Работает на 1С-Битрикс: Корпоративный портал
			<a href="<?= \Itgro\Support\Router::getContactsUrl();; ?>" class="link float-right">Контакты</a>
		</footer>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?= $assetsPath; ?>/js/lib/jquery-1.9.1.min.js"><\/script>')</script>

	<script src="<?= $assetsPath; ?>/js/lib/pickmeup.min.js"></script>
	<script src="<?= $assetsPath; ?>/js/lib/chosen-v1.1.0.min.js"></script>
	<script src="<?= $assetsPath; ?>/js/lib/jquery.textPlaceholder.min.js"></script>
	<script src="<?= $assetsPath; ?>/js/main.js"></script>
</body>
</html>
