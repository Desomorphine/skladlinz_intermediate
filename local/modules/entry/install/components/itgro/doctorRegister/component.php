<?
use Itgro\Exception\EmptyDataException;

(defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

/* @global CMain $APPLICATION */
/* @var CBitrixComponent $this */
/* @var array $arParams */

if (!CModule::IncludeModule("itgro.doctor_entry"))
{
	ShowError("Модуль \"Запись ко врачу\" не установлен");
	return;
}

$arResult = array();

$arParams["SEF_FOLDER"] = "_";

$arUrlTemplates = array(
	"modal.button" => "",
	"service" => "service",
	"salon" => "salon",
	"timetable" => "timetable",
	"comments" => "comments",
	"result" => "result",
);

$arComponentVariables = array();

$arVariables = array();
$arVariableAliases = array();

$engine = new CComponentEngine($this);

if(array_key_exists("STEP", $_REQUEST))
{
	$requestURL = $_REQUEST["STEP"];

	if(strpos($requestURL, "/_/") !== 0)
		$requestURL = "/_/".$requestURL;
}
else
{
	$requestURL = "_";
}


if($requestURL != "_")
	$APPLICATION->RestartBuffer();

print_r($_REQUEST["STEP"]);

$componentPage = $engine->guessComponentPath(
	$arParams["SEF_FOLDER"],
	$arUrlTemplates,
	$arVariables,
	$requestURL
);

if (!is_string($componentPage) || !array_key_exists($componentPage, $arUrlTemplates))
{
	$componentPage = "modal.button";
}

CComponentEngine::InitComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);

$arResult = array(
	"URL_TEMPLATES" => $arUrlTemplates,
	"VARIABLES" => $arVariables,
	"ALIASES" => $arVariableAliases
);

try
{
	$this->IncludeComponentTemplate($componentPage);
}
catch (EmptyDataException $e)
{
	echo $e->getMessage();
}

