<? (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

global $APPLICATION;
//use Itgro\Repository\DoctorTypes;
use Itgro\Repository\Salons;
use Itgro\Util;
use Itgro\Exception\EmptyDataException;


if(array_key_exists("COST", $arParams) && Util::IsIntegerAboveZero($arParams["COST"]))
	$arResult["COST"] = $arParams["COST"];
else
	throw new EmptyDataException();


if(array_key_exists("DURATION", $arParams) && Util::IsIntegerAboveZero($arParams["DURATION"]))
	$arResult["DURATION"] = $arParams["DURATION"];
else
	throw new EmptyDataException();

if($arParams["IS_HELD"] == "N")
{
	$arResult["IS_HELD"] = "N";
}
else
{
	$arFilter = array();

	if(Util::IsNotEmptyString($arParams["LICENSE"]))
		$arFilter["PROPERTY_LICENSE.XML_ID"] = $arParams["LICENSE"];

	if(Util::IsNotEmptyArray($arParams["DOCTOR_TYPE"]))
	{
		$arLogic = array
		(
			"LOGIC" => "OR",
		);

		$arConditions = array();

		foreach($arParams["DOCTOR_TYPE"] as $intDocID)
		{
			$arConditions[]["PROPERTY_DOCTOR_TYPE"] = $intDocID;
		}

		$arLogic = array_merge($arLogic, $arConditions);

		$arFilter[] = $arLogic;
	}

	$arSalons = Salons::GetList($arFilter);

}


$this->IncludeComponentTemplate();
