<? (defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED === true) or die ("Access denied");

global $APPLICATION;
use Itgro\Repository\Services as Services;

if(check_bitrix_sessid() && array_key_exists("SERVICE_ID", $_REQUEST))
{
	$arAjaxResult = Services::getClarifications($_REQUEST["SERVICE_ID"]);

	$APPLICATION->RestartBuffer();
	echo json_encode($arAjaxResult);
	die();
}

$arServices = Services::GetAll();

$arFirstService = reset($arServices);

$arQuestions = Services::getClarifications($arFirstService["ID"]);
$arResultQuestions["LIST"] = $arQuestions;
$arResultQuestions["SERVICE"] = $arFirstService["ID"];

$arResult["SERVICES"] = $arServices;
$arResult["QUESTIONS"] = $arResultQuestions;

$this->IncludeComponenttemplate();