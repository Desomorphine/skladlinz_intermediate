CREATE TABLE `i_support_ticket_message_to_user` (
	`MESSAGE_ID` INT NOT NULL,
	`USER_ID` INT NOT NULL,
	`IS_UNREAD` CHAR NOT NULL DEFAULT 'Y',
	PRIMARY KEY (`MESSAGE_ID`, `USER_ID`),
	INDEX `fk_user_id_idx` (`USER_ID` ASC),
	CONSTRAINT `fk_message_id`
		FOREIGN KEY (`MESSAGE_ID`)
		REFERENCES `portal_itgro`.`b_ticket_message` (`ID`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT,
	CONSTRAINT `fk_user_id`
		FOREIGN KEY (`USER_ID`)
		REFERENCES `portal_itgro`.`b_user` (`ID`)
		ON DELETE RESTRICT
		ON UPDATE RESTRICT
);

CREATE TABLE `i_ticket_status_change_log` (
	`ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`TICKET_ID` INT(11) NOT NULL,
	`STATUS_ID` INT(11) NOT NULL,
	`DATETIME` DATETIME NOT NULL,
	PRIMARY KEY (`ID`),
	INDEX `TICKET_ID_STATUS_ID` (`TICKET_ID`, `STATUS_ID`),
	INDEX `STATUS_ID_DATE_TIME` (`STATUS_ID`, `DATETIME`),
	CONSTRAINT `FK_TICKET_STATUS_CHANGE_LOG_TICKET_ID` FOREIGN KEY (`TICKET_ID`) REFERENCES `b_ticket` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT `FK_TICKET_STATUS_CHANGE_LOG_STATUS_ID` FOREIGN KEY (`STATUS_ID`) REFERENCES `b_ticket_dictionary` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT
);
