<?
/** @var CDBResult $this */
/** @var string $sUrlPath */
/** @var string $strNavQueryString */
/** @var int $nStartPage */
/** @var int $nEndPage */
?>

<? $urlTemplate = $sUrlPath.'?PAGEN_'.$this->NavNum.'=%d'.urldecode($strNavQueryString); ?>

<div class="pagination" id="pagination_<?= $this->NavNum; ?>">
	<? if ($this->NavPageNomer > 1): ?>
		<span class="pagination-nav__prev">Ctrl</span>
		<a href="<?= sprintf($urlTemplate, ($this->NavPageNomer - 1)); ?>" class="pagination-nav__link">предыдущая</a>
	<? else: ?>
		<span class="pagination-nav__prev"></span>
		<span>предыдущая</span>
	<? endif; ?>

	<? if ($nStartPage > 1): ?>
		<a href="<?= sprintf($urlTemplate, 1); ?>" class="pagination__link pagination-nav__first">1</a>
		<? if ($nStartPage > 2): ?>
			&nbsp;...&nbsp;
		<? endif; ?>
	<? endif; ?>

	<? for ($currentPage = $nStartPage; $currentPage <= $nEndPage; $currentPage++): ?>
		<? $class = ($currentPage == $this->NavPageNomer) ? "pagination__link--active" : ""; ?>
		<a href="<?= sprintf($urlTemplate, $currentPage); ?>" class="pagination__link <?= $class; ?>"><?= $currentPage; ?></a>
	<? endfor; ?>

	<? if ($nEndPage < $this->NavPageCount): ?>
		<? if ($nEndPage < ($this->NavPageCount - 1)): ?>
			&nbsp;...&nbsp;
		<? endif; ?>
		<a href="<?= sprintf($urlTemplate, $this->NavPageCount); ?>" class="pagination__link pagination-nav__last"><?= $this->NavPageCount; ?></a>
	<? endif; ?>

	<? if ($this->NavPageNomer < $this->NavPageCount): ?>
		<a href="<?= sprintf($urlTemplate, ($this->NavPageNomer + 1)); ?>" class="pagination-nav__link">следующая</a>
		<span class="pagination-nav__next">Ctrl</span>
	<? else: ?>
		<span>следующая</span>
		<span class="pagination-nav__next"></span>
	<? endif; ?>
</div>
