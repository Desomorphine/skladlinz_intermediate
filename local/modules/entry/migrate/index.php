<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

/*
Дополнительные действия:
1. Переименовать значения свойства-списка "Сервисы" старого инфоблока клиентов в соответсвии со значениями справочника "Категории"
2. Назначить категории пустым клиентам
3. Проставить пользователям категории и клиентов
4. Создать статусы
5. Обновить привязку обращений к статусам
6. Перепривязать обращения с пустой категорией (SELECT * FROM b_ticket WHERE CATEGORY_ID IS NULL OR CATEGORY_ID = 0)

INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','new','N',10,'Новое','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','queue','N',20,'Ожидание','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','pending','N',30,'Решение','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','information_request','N',40,'Уточнение','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','development','N',50,'Проект','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','verification','N',60,'Приемка работ','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','control','N',70,'Контроль','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','negotiation','N',80,'Согласование','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','closed','N',90,'Завершено','',NULL,'ticket',NULL,NULL);
INSERT INTO `b_ticket_dictionary` (`FIRST_SITE_ID`,`C_TYPE`,`SID`,`SET_AS_DEFAULT`,`C_SORT`,`NAME`,`DESCR`,`RESPONSIBLE_USER_ID`,`EVENT1`,`EVENT2`,`EVENT3`) VALUES ('s1','S','deleted','N',100,'Удалено','',NULL,'ticket',NULL,NULL);

INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (50, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (50, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (51, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (51, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (52, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (52, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (53, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (53, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (54, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (54, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (55, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (55, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (56, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (56, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (57, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (57, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (58, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (58, 'co');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (59, 's1');
INSERT INTO `b_ticket_dictionary_2_site` (`DICTIONARY_ID`, `SITE_ID`) VALUES (59, 'co');

UPDATE `b_ticket` SET `STATUS_ID` = 58 WHERE `STATUS_ID` IN (0, 47, 9, 10) OR `STATUS_ID` IS NULL;
UPDATE `b_ticket` SET `STATUS_ID` = 51 WHERE `STATUS_ID` IN (7, 42);
UPDATE `b_ticket` SET `STATUS_ID` = 54 WHERE `STATUS_ID` IN (45, 31, 44);
UPDATE `b_ticket` SET `STATUS_ID` = 50 WHERE `STATUS_ID` IN (41);
UPDATE `b_ticket` SET `STATUS_ID` = 55 WHERE `STATUS_ID` IN (37, 46);
UPDATE `b_ticket` SET `STATUS_ID` = 59 WHERE `STATUS_ID` IN (49);
UPDATE `b_ticket` SET `STATUS_ID` = 53 WHERE `STATUS_ID` IN (36, 43);

UPDATE b_uts_support SET UF_TICKET_CLIENT = UF_TICKET_CLIENT + 101 WHERE UF_TICKET_CLIENT IS NOT NULL AND UF_TICKET_CLIENT > 0;
*/
//
///** @global CDatabase $DB */
//
//CModule::IncludeModule("iblock");
//CModule::IncludeModule("itgro.support");
//
//global $DB;
//
//$iblockID = COption::GetOptionInt("itgro.support", "clients_iblock_id", 0);
//$propertyCategoryID = COption::GetOptionInt(ADMIN_MODULE_NAME, "clients_property_category_id", 0);
//
//if (\Itgro\Util::IsIntegerAboveZero($iblockID) && \Itgro\Util::IsIntegerAboveZero($propertyCategoryID))
//{
//	$arCategories = \Itgro\Util::SetArrayKeysFromField(\Itgro\Support\Directory\Category::GetList(), "NAME");
//
//	$obUser = new CUser();
//
//	$obElement = new CIBlockElement();
//
//	$dbElements = $obElement->GetList(array("ID" => "ASC"), array("IBLOCK_ID" => 12), false, false, array("*", "PROPERTY_*"));
//
//	while ($arElement = $dbElements->GetNext(false, false))
//	{
//		$arElementCategories = array($arCategories["ВЕБ"]["ID"]);
//
//		if (\Itgro\Util::IsNotEmptyArray($arElement["PROPERTY_113"]))
//		{
//			foreach ($arElement["PROPERTY_113"] as $categoryName)
//			{
//				if (array_key_exists($categoryName, $arCategories))
//				{
//					$arElementCategories[] = $arCategories[$categoryName]["ID"];
//				}
//				else
//				{
//					ShowError("Категория \"".$categoryName."\" не найдена");
//				}
//			}
//		}
//
//		$clientID = $obElement->Add(
//			array(
//				"NAME" => $arElement["NAME"],
//				"ACTIVE" => $arElement["ACTIVE"],
//				"IBLOCK_ID" => $iblockID,
//				"PROPERTY_VALUES" => array(
//					$propertyCategoryID => array_unique($arElementCategories),
//				)
//			)
//		);
//
//		if (\Itgro\Util::IsIntegerAboveZero($clientID))
//		{
//			$dbUsers = $obUser->GetList($by = "id", $order = "asc", array("UF_CLIENT" => $arElement["ID"]), array("FIELDS" => array("ID")));
//
//			while ($arUser = $dbUsers->GetNext(true, false))
//			{
//				$obUser->Update($arUser["ID"], array("UF_CRM_CLIENT" => array($clientID)));
//			}
//
//			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
//			$dbTickets = CTicket::GetList(
//				$sort_by = "ID",
//				$sort_order = "ASC",
//				array("UF_CRM_CLIENT" => $arElement["ID"]),
//				$is_filtered,
//				"Y",
//				"N",
//				"N",
//				false,
//				array()
//			);
//
//			while ($arTicket = $dbTickets->GetNext(true, false))
//			{
//				$DB->Update(
//					"b_uts_support",
//					array(
//						"UF_CRM_CLIENT" => "'".$clientID."'"
//					),
//					"WHERE UF_CRM_CLIENT = '".$arElement["ID"]."'"
//				);
//			}
//		}
//		else
//		{
//			ShowError($arElement["ID"].": ".$obElement->LAST_ERROR);
//		}
//	}
//}
//else
//{
//	ShowError("Модуль \"Интерфейс техподдержки\" не настроен");
//}
//
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");