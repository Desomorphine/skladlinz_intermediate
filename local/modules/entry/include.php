<?
require_once(dirname(__FILE__)."/prolog.php");

CModule::AddAutoloadClasses(
	'entry',
	array(
		'Itgro\Util' => 'lib/util.php',
		'Itgro\Exceptions\EmptyDataException' => 'lib/exception/empty_data.php',
		'Focus\UserDataStorage' => 'lib/focus/userDataStorage.php',
		'Focus\Storage' => 'lib/focus/storage.php',
		'Focus\Component' => 'lib/focus/component.php',

		#region Directory
		'Focus\Directory\Base' => 'lib/focus/directory/base.php',
		'Focus\Directory\Synchronized' => 'lib/focus/directory/synchronized.php',
		'Focus\Directory\Salon' => 'lib/focus/directory/salon.php',
		'Focus\Directory\Service' => 'lib/focus/directory/service.php',
		'Focus\Directory\Doctor' => 'lib/focus/directory/doctor.php',
		'Focus\Directory\Timetable' => 'lib/focus/directory/timetable.php',
		#endregion

		#region Structure
		'Focus\Directory\Structure\Base' => 'lib/focus/directory/structure/base.php',
		'Focus\Directory\Structure\Salon' => 'lib/focus/directory/structure/salon.php',
		'Focus\Directory\Structure\Service' => 'lib/focus/directory/structure/service.php',
		'Focus\Directory\Structure\Doctor' => 'lib/focus/directory/structure/doctor.php',
		'Focus\Directory\Structure\Timetable' => 'lib/focus/directory/structure/timetable.php',
		#endregion

		#region Repository
		'Focus\Repository\Base' => 'lib/focus/repository/base.php',
		'Focus\Repository\Salon' => 'lib/focus/repository/salon.php',
		'Focus\Repository\Service' => 'lib/focus/repository/service.php',
		'Focus\Repository\Questions' => 'lib/focus/repository/question.php',
		'Focus\Repository\Answers' => 'lib/focus/repository/answer.php',
		'Focus\Repository\Doctor' => 'lib/focus/repository/doctor.php',
		#endregion
	)
);
