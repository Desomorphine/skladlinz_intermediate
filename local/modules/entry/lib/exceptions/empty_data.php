<?
namespace Itgro\Exception;

class EmptyDataException extends \Exception
{
    public function __construct($message = "", $code = 0, \Exception $previous = null) {

	    if(!Util::IsNotEmptyString($message))
		    $message = "Произошла ошибка, пожалуйста пройдите процесс записи с начала.";

        parent::__construct($message, $code, $previous);
    }
}
