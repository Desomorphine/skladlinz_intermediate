<?php

namespace Focus;

use Itgro\App;
use Itgro\Http;
use CBitrixComponent;
use Itgro\Request\Request;

abstract class Component extends CBitrixComponent
{
	protected $save_error;

	/**
	 * @return array
	 */
	abstract function save ();

	/**
	 * @return array
	 */
	abstract function get ($id);

	/**
	 * @return array
	 */
	abstract function all ();

	/**
	 * @return array
	 */
	public function action ()
	{
		if (Request::is_post())
		{
			if ($this->save ())
			{
				return ['status' => 'ok'];
			}

			return ['status' => 'error', 'message' => $this->save_error];
		}
		else if (Request::is_get() && array_key_exists('ID', $this->arParams) && $this->arParams['ID'] > 0)
		{
			return $this->get($this->arParams['ID']);
		}
		else
		{
			return $this->all();
		}
	}

	/**
	 * @return mixed
	 */
	public function executeComponent()
	{
		if (Request::get()->has('ID'))
		{
			$this->arParams['ID'] = Request::get()->get('ID');
		}

		$this->arResult = $this->action();

		if (App::ajax())
		{
			Http::send_json($this->arResult);
		}

		return parent::executeComponent();
	}
}
