<?php
namespace Focus\Directory;

use COption;
use Focus\Directory\Structure\Doctor as Structure;

/**
 *
 * @param string $structure link to structure class for modify result
 * @method Structure[] getList
 */
class Doctor extends Synchronized
{
	protected $structure = 'Focus\Directory\Structure\Doctor';
	protected $repository = 'Focus\Repository\Doctor';

	protected $alias = [
		'getList' => 'BackDoctorsList',
	];

	protected $map = [
		'id' => 'XML_ID',
		'name' => 'NAME',
		'services' => 'PROPERTY_SERVICES',
	];

	/**
	 * Return iblock id of directory entity
	 *
	 * @return int
	 */
	public function getDirectoryId()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "doctor_types.iblock_id", null);
	}

	/**
	 * Return url of WSDL service
	 *
	 * @return string
	 */
	public function getSoapUrl()
	{
		return 'http://5.172.10.121:8087/so_central/ws/saloninfo.1cws?wsdl';
	}
}
