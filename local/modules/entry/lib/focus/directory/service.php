<?php
namespace Focus\Directory;

use COption;
use Focus\Directory\Structure\Service as Structure;

/**
 *
 * @param string $structure link to structure class for modify result
 * @method Structure[] getList
 */
class Service extends Synchronized
{
	protected $structure = 'Focus\Directory\Structure\Service';
	protected $repository = 'Focus\Repository\Service';

	protected $alias = [
		'getList' => 'BackServList',
	];

	protected $map = [
		'id' => 'XML_ID',
		'name' => 'NAME',
		'license' => 'PROPERTY_LICENSE',
		'price' => 'PROPERTY_PRICE',
		'duration' => 'PROPERTY_DURATION',
		'duration_ex' => 'PROPERTY_DURATION_EX',
	];

	/**
	 * Return iblock id of directory entity
	 *
	 * @return int
	 */
	public function getDirectoryId()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "services.iblock_id", null);
	}

	/**
	 * Return url of WSDL service
	 *
	 * @return string
	 */
	public function getSoapUrl()
	{
		return 'http://5.172.10.121:8087/so_central/ws/saloninfo.1cws?wsdl';
	}
}
