<?php
namespace Focus\Directory\Structure;

class Timetable extends Base
{
	protected $map = [
		'Id' => "id",
		'Name' => "name",
		'License' => "license",
		'Price' => "price",
		'Duration' => "duration",
		'DurationExp' => "duration_ex",
	];

	public static function soap($result)
	{
		$result = $result->return;

		$arObjects = [];

		foreach ((array) $result->Service as $obj)
		{
			$arObjects[] = new static($obj);
		}

		return $arObjects;
	}
}
