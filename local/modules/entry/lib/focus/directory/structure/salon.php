<?php
namespace Focus\Directory\Structure;

class Salon extends Base
{
	protected $map = [
		'Id' => "id",
		'Name' => "name",
		'License' => "license",
		'Adress' => "address",
	];

	public static function soap($result)
	{
		$result = $result->return;

		$arObjects = [];

		foreach ((array) $result->Salon as $obj)
		{
			$arObjects[] = new static($obj);
		}

		return $arObjects;
	}

	public function __construct ($object)
	{
		parent::__construct($object);

		$this->doctors = [];

		foreach ($object->Doctors->DoctorType as $key => $doctor)
		{
			if (is_object($doctor))
			{
				$this->attributes['doctors'][] = $doctor->Id;
			}
			else
			{
				$this->attributes['doctors'][] = $doctor;
			}
		}
	}
}
