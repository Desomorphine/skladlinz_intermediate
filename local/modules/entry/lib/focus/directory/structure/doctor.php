<?php
namespace Focus\Directory\Structure;

class Doctor extends Base
{
	protected $map = [
		'Id' => "id",
		'Name' => "name",
	];

	public static function soap($result)
	{
		$result = $result->return;

		$arObjects = [];

		foreach ((array) $result->DoctorType as $obj)
		{
			$arObjects[] = new static($obj);
		}

		return $arObjects;
	}

	public function __construct ($object)
	{
		parent::__construct($object);

		$this->services = [];

		foreach ($object->Services->Service as $key => $service)
		{
			if (is_object($service))
			{
				$this->attributes['services'][] = $service->Id;
			}
			else
			{
				$this->attributes['services'][] = $service;
			}
		}
	}
}
