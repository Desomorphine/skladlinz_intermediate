<?php
namespace Focus\Directory\Structure;

use BadMethodCallException;
use Bitrix\Main\NotImplementedException;
use stdClass;

abstract class Base
{
	protected $attributes = [];

	protected $map = [];

	/**
	 * @param $result
	 * @throws NotImplementedException
	 */
	public static function soap($result)
	{
		// Static methods should not be abstract
		throw new NotImplementedException();
	}

	public function __construct ($object)
	{
		foreach ($this->map as $key => $attr)
		{
			if (isset($object->{$key}))
			{
				$this->{$attr} = $object->{$key};
			}
		}
	}

	public function __set ($key, $value)
	{
		$this->attributes[$key] = $value;
	}

	public function __get ($key)
	{
		return $this->attributes[$key];
	}

	public function __isset ($key)
	{
		return isset($this->attributes[$key]);
	}

	public function __unset ($key)
	{
		unset($this->attributes[$key]);
	}

	public function getId ()
	{
		return $this->id;
	}
}
