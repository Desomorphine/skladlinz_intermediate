<?php
namespace Focus\Directory;
use Bitrix\Main\NotSupportedException;
use Focus\Repository\Base as Repository;
use Focus\Directory\Structure\Base as Structure;

/**
 * Class DirectoryInterface
 * @package Focus
 *
 * @method Structure[] getList
 *
 * @param [] $map
 */
abstract class Synchronized extends Base
{
	protected $repository = null;

	protected $map = [];

	#region IBlocks

	/**
	 * Return iblock id of directory entity
	 *
	 * @return int
	 */
	abstract public function getDirectoryId();

	public function sync ()
	{
		$repo = $this->getRepository();

		$remote = $this->getList();
		$local = $repo::getList();

		$this->checkLocal($remote, $local);
		$this->checkRemote($remote, $local);

		return false;
	}

	protected function checkLocal ($remotes, $locals)
	{
		$difference = array_diff(
			array_pluck($remotes, 'id'),
			array_pluck($locals, 'XML_ID')
		);

		$news = array_find($remotes, function ($item) use ($difference)
		{
			return in_array($item->id, $difference);
		});

		foreach ($news as $new)
		{
			$this->add($new);
		}
	}

	protected function add (Structure $element)
	{
		$repo = $this->getRepository();

		if ($exist = $repo->findXml($element->getId()))
		{
			if (!yes($exist['ACTIVE']))
			{
				$repo->update($exist['ID'], ['ACTIVE' => 'Y']);
			}

			return;
		}

		$fields = [];

		foreach ($this->map as $r => $l)
		{
			if (isset($element->$r))
			{
				if (preg_match('/PROPERTY_(.*)/', $l, $match))
				{
					$fields['PROPERTY_VALUES'][$match[1]] = $repo->prepareProperty($match[1], $element->$r);
					continue;
				}

				$fields[$l] = $element->$r;
			}
		}

		$repo->add($fields);
	}

	protected function checkRemote ($remotes, $locals)
	{
		$difference = array_diff(
			array_pluck($locals, 'XML_ID'),
			array_pluck($remotes, 'id')
		);

		$news = array_find($locals, function ($item) use ($difference)
		{
			return in_array($item['XML_ID'], $difference);
		});

		foreach ($news as $new)
		{
			$this->remove($new['ID']);
		}
	}

	protected function remove ($id)
	{
		$repo = $this->getRepository();

		$repo->update($id, ['ACTIVE' => 'N']);
	}

	/**
	 * @return Repository
	 * @throws NotSupportedException
	 */
	protected function getRepository ()
	{
		if (!is_subclass_of($this->repository, 'Focus\Repository\Base'))
		{
			throw new NotSupportedException();
		}

		return new $this->repository();
	}

	#endregion
}
