<?php
namespace Focus\Directory;

use COption;
use Focus\Directory\Structure\Salon as Structure;

/**
 *
 * @param string $structure link to structure class for modify result
 * @method Structure[] getList
 */
class Salon extends Synchronized
{
	protected $structure = 'Focus\Directory\Structure\Salon';
	protected $repository = 'Focus\Repository\Salon';

	protected $alias = [
		'getList' => 'BackSalonsList',
	];

	protected $map = [
		'id' => 'XML_ID',
		'name' => 'NAME',
		'license' => 'PROPERTY_LICENSE',
		'address' => 'PROPERTY_ADDRESS',
		'doctors' => 'PROPERTY_DOCTORS',
	];

	/**
	 * Return iblock id of directory entity
	 *
	 * @return int
	 */
	public function getDirectoryId()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "salons.iblock_id", null);
	}

	/**
	 * Return url of WSDL service
	 *
	 * @return string
	 */
	public function getSoapUrl()
	{
		return 'http://5.172.10.121:8087/so_central/ws/saloninfo.1cws?wsdl';
	}
}
