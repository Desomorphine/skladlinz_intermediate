<?php
namespace Focus\Directory;

use BadFunctionCallException;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\NotSupportedException;
use Focus\Repository\Base as Repository;
use Focus\Directory\Structure\Base as Structure;
use SoapClient, SoapFault;

/**
 * Class DirectoryInterface
 * @package Focus
 *
 * @method Structure[] getList
 *
 * @param SoapClient $client
 * @param [] $map
 */
abstract class Base
{
	protected $client;

	protected $structure = null;

	protected $alias = [];

	#region SOAP

	/**
	 * Return url of WSDL service
	 *
	 * @return string
	 */
	abstract public function getSoapUrl();

	/**
	 * Returns SOAP connection parameters
	 *
	 * @return []
	 */
	public function getSoapParams()
	{
		return [
			'login' => "Web Store",
			'password' => "123",
			'proxy_host' => "http://5.172.10.121:8087/salonInfo",
			'proxy_login' => "Web Store",
			'proxy_password' => "123",
			'stream_context' => stream_context_create([
				'http'=> [
					'user_agent' => 'PHPSoapClient'
				]
			]),
			'cache_wsdl' => WSDL_CACHE_NONE
		];
	}

	/**
	 * Simple check
	 *
	 * @param $needle
	 * @return bool
	 */
	protected function functionExists ($needle)
	{
		$list = $this->getClient()->__getFunctions();

		foreach ($list as $funcName)
		{
			$funcName = preg_split("/[\s\(]/", $funcName)[1];

			if ($needle == $funcName)
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Check function os alias
	 *
	 * @param $action
	 * @return mixed
	 * @throws ArgumentException
	 */
	protected function resolveFunction ($action)
	{
		if (array_key_exists($action, $this->alias))
		{
			return $this->alias[$action];
		}

		return $action;
	}

	/**
	 * Create if not exists and return soap client
	 *
	 * @return SoapClient
	 */
	protected function getClient ()
	{
		if (!isset($this->client))
		{
			$this->client = new SoapClient($this->getSoapUrl(), $this->getSoapParams());
		}

		return $this->client;
	}

	/**
	 * @param string $action
	 * @param [] $arguments
	 * @param [] $options
	 * @return mixed|null
	 * @throws ArgumentException
	 * @throws BadFunctionCallException
	 */
	public function action ($action, $arguments = [], $options = [])
	{
		try
		{
			$function = $this->resolveFunction($action);

			if ($this->functionExists($function))
			{
				$result = $this->getClient()->__soapCall($function, (array) $arguments, $options);

				if (is_subclass_of($this->structure, 'Focus\Directory\Structure\Base'))
				{
					return call_user_func_array([$this->structure, 'soap'], [$result]);
				}

				return $result;
			}
			else
			{
				throw new BadFunctionCallException('Undefined soap function ' . $function);
			}
		}
		catch (SoapFault $e)
		{
			return null;
		}
	}

	#endregion

	#region Overload

	/**
	 * @param string $method
	 * @param [] $args
	 * @return mixed|null
	 */
	public function __call ($method, $args)
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array([$this, $method], array_shift($args), array_shift($args));
		}

		return $this->action($method, array_shift($args), array_shift($args));
	}

	#endregion
}
