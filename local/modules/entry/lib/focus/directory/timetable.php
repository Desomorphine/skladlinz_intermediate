<?php
namespace Focus\Directory;

use COption;
use Focus\Directory\Structure\Timetable as Structure;

/**
 *
 * @param string $structure link to structure class for modify result
 * @method Structure[] getList
 */
class Timetable extends Base
{
	protected $structure = 'Focus\Directory\Structure\Timetable';

	protected $alias = [
		'getList' => 'Schedule',
	];

	/**
	 * Return url of WSDL service
	 *
	 * @return string
	 */
	public function getSoapUrl()
	{
		return 'http://5.172.10.121:8087/so_central/ws/saloninfo.1cws?wsdl';
	}

}
