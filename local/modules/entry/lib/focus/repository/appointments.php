<?
namespace Focus\Repository;

use COption;

class Appointments extends Base
{
	static $filter = [];

	static $fields = [
		"ID",
		"IBLOCK_ID",
		"NAME",
		"PROPERTY_USER",
		"PROPERTY_REFINEMENTS",
		"PROPERTY_SALON",
		"PROPERTY_DOCTOR_TYPE",
		"PROPERTY_DATE",
		"PROPERTY_DURATION",
		"PROPERTY_NAME_OF_PATIENT",
		"PROPERTY_PHONE",
		"PROPERTY_EMAIL",
		"PROPERTY_COMMENT",
		"PROPERTY_DOCTOR_NAME",
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "appointment.iblock_id");
	}
}
