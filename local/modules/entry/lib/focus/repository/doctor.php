<?
namespace Focus\Repository;

use COption;

class Doctor extends Base
{
	static $filter = [];

	static $fields = [
		'ID',
		'IBLOCK_ID',
		'NAME',
		'XML_ID',
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "doctor_types.iblock_id");
	}
}
