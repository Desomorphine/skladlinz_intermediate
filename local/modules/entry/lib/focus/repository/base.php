<?php

namespace Focus\Repository;

use Bitrix\Main\DB\Paginator;
use Bitrix\Main\NotImplementedException;
use Bitrix\Security\LogicException;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CIBlockResult;
use CModule;

abstract class Base
{
	/**
	 * @var array
	 */
	protected static $filter = [];

	/**
	 * @var array
	 */
	protected static $fields = [];

	protected static $properties = [];

	protected static $enums = [];

	/**
	 * @return int
	 * @throws NotImplementedException
	 */
	public static function getIBlockID()
	{
		throw new NotImplementedException();
	}

	/**
	 * @return CIBlockElement
	 */
	public static function getProvider ()
	{
		if (CModule::IncludeModule('iblock'))
		{
			return new CIBlockElement();
		}

		throw new LogicException();
	}

	/**
	 * @return CIBlockProperty
	 */
	public static function getPropertyProvider ()
	{
		if (CModule::IncludeModule('iblock'))
		{
			return new CIBlockProperty();
		}

		throw new LogicException();
	}

	/**
	 * @return CIBlockPropertyEnum
	 */
	public static function getPropertyEnumProvider ()
	{
		if (CModule::IncludeModule('iblock'))
		{
			return new CIBlockPropertyEnum();
		}

		throw new LogicException();
	}

	/**
	 * @return CIBlockELement
	 */

	/**
	 * @param array $filter
	 * @param array $order
	 * @param bool $pagen
	 * @param bool $group
	 * @return array
	 * @throws LogicException
	 * @throws NotImplementedException
	 */
	public static function getList($filter = [], $order = [], $pagen = false, $group = false)
	{
		$result = [];

		$filter = array_merge($filter, ["IBLOCK_ID" => static::getIBlockID()], static::$filter);

		/** @var CIBlockElement $provider */
		$provider = static::getProvider();

		$elements = $provider->GetList(
			$order, $filter, $group, $pagen, static::$fields
		);

		while($element = static::fetch($elements))
		{
			$result[] = $element;
		}

		return $result;
	}

	/**
	 * @param CIBlockResult $db
	 * @return array
	 */
	protected static function fetch (CIBlockResult $db)
	{
		$el = $db->Fetch();

		if (!$el)
		{
			return false;
		}

		$result = [];

		foreach ($el as $field => $value)
		{
			$propName = static::_propertyName($field);

			if ($propName === 0)
			{
				$result[$field] = $value;
			}
			else if ($propName !== 1)
			{
				$result[$propName] = static::formatProperty($propName, $value);
			}

		}

		return $result;
	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function getByID ($id)
	{
		$elements = static::GetList(array("=ID" => $id));

		if (!empty($elements))
		{
			return reset($elements);
		}

		return false;
	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function findXml ($id)
	{
		$elements = static::GetList(array("=XML_ID" => $id));

		if (!empty($elements))
		{
			return reset($elements);
		}

		return false;
	}

	/**
	 * @param array $arSort
	 * @return array
	 */
	public static function all ($order = [])
	{
		return static::GetList(["ACTIVE" => "Y"], $order);
	}

	/**
	 * @param $fields
	 * @return bool|int
	 * @throws LogicException
	 * @throws NotImplementedException
	 */
	public function add ($fields)
	{
		return static::getProvider()->Add(array_merge($fields, ["IBLOCK_ID" => static::getIBlockID()]));
	}

	/**
	 * @param $id
	 * @param $fields
	 * @return bool
	 * @throws LogicException
	 */
	public function update ($id, $fields)
	{
		return static::getProvider()->Update($id, $fields);
	}

	private static function _propertyName ($name)
	{
		if (preg_match('/^PROPERTY_(.*?)_VALUE$/', $name, $match))
		{
			return $match[1];
		}
		else if (preg_match('/^PROPERTY_(.*?)/', $name, $match))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	private static function findPropertyEnum ($name, $value)
	{
		$property = static::getProperty($name);
		$enums = static::getPropertyEnums($name);
		$value = (array) $value;

		$result = [];

		foreach ($value as $val)
		{
			$enum = array_find($enums, function ($item) use ($val)
			{
				if (is_bool($val))
				{
					if (yes($item['XML_ID']) && $val)
					{
						return true;
					}

					if (no($item['XML_ID']) && !$val)
					{
						return true;
					}
				}

				return ($item["ID"] == $val || $item['XML_ID'] == $val || $item['VALUE'] == $val);
			});

			if (!empty($enum))
			{
				$result[] = reset($enum);
			}
		}

		if (no($property['MULTIPLE']))
		{
			$result = reset($result);
		}

		return $result;
	}

	/**
	 * @param $name
	 * @param $value
	 */
	public static function prepareProperty ($name, $value)
	{
		$property = static::getProperty($name);

		if ($property['PROPERTY_TYPE'] == 'L')
		{
			$result = static::findPropertyEnum($name, $value);

			if (no($property['MULTIPLE']))
			{
				return $result['ID'];
			}

			return array_pluck($result, 'ID');
		}
		else if ($property['PROPERTY_TYPE'] == 'E')
		{
			$db = static::getProvider()->GetList([], ['IBLOCK_ID' => $property["LINK_IBLOCK_ID"], 'XML_ID' => $value]);

			$result = [];

			while ($element = $db->fetch())
			{
				$result[] = $element['ID'];
			}

			if (no($property['MULTIPLE']))
			{
				$result = reset($result);
			}

			return $result;
		}
		else
		{
			return $value;
		}
	}

	public static function formatProperty ($name, $value)
	{
		$property = static::getProperty($name);
		if ($property['PROPERTY_TYPE'] == 'L')
		{
			$result = static::findPropertyEnum($name, $value);

			if (no($property['MULTIPLE']))
			{
				return $result['VALUE'];
			}

			return array_pluck($result, 'VALUE');
		}
		else
		{
			return $value;
		}
	}

	public static function getProperty ($code)
	{
		if (!isset(static::$properties[$code]))
		{
			$props = static::getPropertyProvider();
			$db = $props->GetList([], ['CODE' => $code, 'IBLOCK_ID' => static::getIBlockID()]);

			if ($prop = $db->fetch())
			{
				static::$properties[$code] = $prop;
			}
		}

		return static::$properties[$code];
	}

	public static function getPropertyEnums ($code)
	{
		if (!isset(static::$enums[$code]))
		{
			$enums = static::getPropertyEnumProvider();
			$db = $enums->GetList([], ['CODE' => $code, 'IBLOCK_ID' => static::getIBlockID()]);

			while ($enum = $db->fetch())
			{
				static::$enums[$code][] = $enum;
			}
		}

		return static::$enums[$code];
	}
}
