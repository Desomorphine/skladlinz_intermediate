<?
namespace Focus\Repository;

use COption;

class Salon extends Base
{
	static $filter = [];

	static $fields = [
		"ID",
		"IBLOCK_ID",
		"NAME",
		"DETAIL_TEXT",
		"PROPERTY_ADDRESS",
		"PROPERTY_DOCTOR_TYPE",
		"PROPERTY_LICENSE",
		"PROPERTY_PHONE",
		"PROPERTY_WORKTIME",
		"PROPERTY_TRAMS",
		"PROPERTY_BUSES",
		"PROPERTY_CABS",
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "salons.iblock_id");
	}
}
