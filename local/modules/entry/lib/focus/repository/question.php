<?
namespace Focus\Repository;

use COption;

class Questions extends Base
{
	static $filter = [];

	static $fields = [
		'ID',
		'IBLOCK_ID',
		'NAME',
		'XML_ID',
		'PROPERTY_SERVICE',
		'PROPERTY_CLARIFICATION',
		'CODE'
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "questions.iblock_id");
	}
}
