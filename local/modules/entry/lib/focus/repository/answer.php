<?
namespace Focus\Repository;

use COption;

class Answers extends Base
{
	static $filter = [];

	static $fields = [
		'ID',
		'IBLOCK_ID',
		'NAME',
		'XML_ID',
		'PROPERTY_QUESTION',
		'PROPERTY_DOCTOR_TYPE',
		'PROPERTY_DURATION',
		'PROPERTY_COST',
		'PROPERTY_IS_HELD',
		'PROPERTY_LICENSE',
		'PROPERTY_CLARIFICATION',
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "answers.iblock_id");
	}
}
