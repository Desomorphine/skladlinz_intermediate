<?
namespace Focus\Repository;

use COption;
use Itgro\Util;

class Service extends Base
{
	static $filter = [];

	static $fields = [
		'ID',
		'IBLOCK_ID',
		'NAME',
		'XML_ID',
		'PROPERTY_XML_ID',
		'PROPERTY_LICENSE',
		'PROPERTY_PRICE',
		'PROPERTY_DURATION',
		'PROPERTY_DETAIL_LINK',
		'PROPERTY_COST',
		'DETAIL_TEXT'
	];

	public static function getIBlockID()
	{
		return COption::GetOptionInt("itgro.doctor_entry", "services.iblock_id");
	}

	public static function getClarifications($questionID)
	{
		$arQuestions = Questions::GetList(array("PROPERTY_SERVICE_VALUE" => $questionID));

		if(!Util::IsNotEmptyArray($arQuestions))
			return false;

		foreach ($arQuestions as &$arQuestion)
		{
			$arQuestion = array_only($arQuestion, array("ID", "NAME", "CODE"));

			$arAnswers = Answers::GetList(array("PROPERTY_QUESTION_VALUE" => $arQuestion["ID"]));

			$arQuestion["ANSWERS"] = array_assoc($arAnswers, "ID");
		}
		unset($arQuestion);

		return $arQuestions;
	}
}
