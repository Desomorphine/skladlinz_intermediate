<?
namespace Focus;

class UserDataStorage {

	public $user = 0;
	public $service = 0;
	public $clarifications = array();
	public $salon = 0;
	public $doctorType = 0;
	public $appointmentTime = "";
	public $cost = 0;
	public $duration = 0;
	public $enlistedName = "";
	public $enlistedPhone = "";
	public $enlistedEmail = "";
	public $comment = "";
	public $appointerName = "";


	function __construct()
	{
		if(!array_key_exists("DOCTOR_ENTRY", $_SESSION))
		{
			$_SESSION["DOCTOR_ENTRY"] = array();
		}
		else
		{
			foreach ($_SESSION["DOCTOR_ENTRY"] as $name => $value)
			{
				if(self::isCorrectProperty($name))
				{
					$this->$name = $value;
				}
			}
		}
	}

	private static function isCorrectProperty($name)
	{
		if(in_array($name, get_class_vars(__CLASS__)))
		{
			return true;
		}

		return false;
	}

	public function save()
	{
		$this->__destruct();
	}

	function __destruct()
	{
		foreach (get_class_vars(__CLASS__) as $name => $val)
		{
			$_SESSION["DOCTOR_ENTRY"][$name] = $this->$name;
		}
	}
}
