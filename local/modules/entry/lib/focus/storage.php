<?
namespace Focus;

class Storage
{
	const SESSION_KEY = 'DOCTOR_ENTRY';

	private static $instance = null;

	private static $fields = [
		'user' => 'int',
		'service' => 'int',
		'clarifications' => 'arr',
		'salon' => 'int',
		'doctorType' => 'int',
		'appointmentTime' => 'int',
		'cost' => 'int',
		'duration' => 'int',
		'enlistedName' => 'str',
		'enlistedPhone' => 'str',
		'enlistedEmail' => 'str',
		'comment' => 'str',
		'appointerName' => 'str',
	];

	private $data;

	public function __construct ()
	{
		if(!array_key_exists(static::SESSION_KEY, $_SESSION))
		{
			$_SESSION[static::SESSION_KEY] = array();
		}

		$this->data = &$_SESSION[static::SESSION_KEY];
	}

	/**
	 * @return Storage
	 */
	private static function getInstance ()
	{
		if (is_null(static::$instance))
		{
			static::$instance = new static();
		}

		return static::$instance;
	}

	public static function get ($key)
	{
		if (static::getInstance()->has($key))
		{
			return static::getInstance()->data[$key];
		}
		else if (static::fieldType($key) == 'arr')
		{
			return [];
		}

		return null;
	}

	public static function put ($key, $value)
	{
		if (static::checkValue($key, $value))
		{
			static::getInstance()->data[$key] = $value;
			return true;
		}

		return false;
	}

	public static function push ($key, $value)
	{
		if (static::fieldType($key) == 'arr')
		{
			if (!is_array(static::getInstance()->data[$key]))
			{
				static::getInstance()->data[$key] = [];
			}

			array_push(static::getInstance()->data[$key], $value);
		}

		return false;
	}

	public static function clear ($key)
	{
		if (static::has($key))
		{
			unset(static::getInstance()->data[$key]);

			if (static::fieldType($key) == 'arr')
			{
				static::getInstance()->data[$key] = [];
			}
		}
	}

	public static function has ($key)
	{
		if (static::fieldType($key) == 'arr')
		{
			return is_array(static::getInstance()->data[$key]) && count(static::getInstance()->data[$key]) > 0;
		}

		return array_key_exists($key, static::getInstance()->data);
	}

	private static function fieldType ($key)
	{
		if (array_key_exists($key, static::$fields))
		{
			return static::$fields[$key];
		}

		return false;
	}

	private static function checkValue ($key, $value)
	{
		$type = static::fieldType($key);

		switch ($type)
		{
			case 'int':

				return is_int($value);

				break;

			case 'str':

				return is_string($value);

				break;

			case 'arr':

				return is_array($value);

				break;
		}

		return false;
	}
}
