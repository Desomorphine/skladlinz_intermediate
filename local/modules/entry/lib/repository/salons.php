<?
namespace Itgro\Repository;

use Itgro\Util;

class Salons
{
	/**
	 * @param array $arFilter
	 * @param array $arSort
	 * @param array|bool $arParams
	 *
	 * @return array
	 */
	public static function GetList (array $arFilter = array(), array $arSort = array(), $arParams = false)
	{
		\CModule::IncludeModule("iblock");
		$arSalons = array();

		$arDefaultFilter = array(
			"IBLOCK_ID" => \COption::GetOptionInt("itgro.doctor_entry", "salons.iblock_id")
		);

		$arFilter = array_merge($arFilter, $arDefaultFilter);

		$rsSalons = \CIBlockElement::GetList(
			$arSort, $arFilter, false, $arParams,
			array(
				"ID",
				"IBLOCK_ID",
				"NAME",
				"PROPERTY_ADDRESS",
				"PROPERTY_DOCTOR_TYPE",
				"PROPERTY_LICENSE",
			)
		);

		while($arSalon = $rsSalons->getNext(false, false))
		{
			$arSalon = self::FetchModifier($arSalon);

			$arSalons[] = $arSalon;
		}

		return $arSalons;
	}

	private static function FetchModifier($arSalon)
	{
		$arResultSalon = array();

		$arResultSalon["ID"] = $arSalon["ID"];
		$arResultSalon["IBLOCK_ID"] = $arSalon["IBLOCK_ID"];
		$arResultSalon["NAME"] = $arSalon["NAME"];

		if(array_key_exists("PROPERTY_ADDRESS_VALUE", $arSalon))
			$arResultSalon["ADDRESS"] = $arSalon["PROPERTY_ADDRESS_VALUE"];

		if(array_key_exists("PROPERTY_DOCTOR_TYPE_VALUE", $arSalon))
			$arResultSalon["DOCTOR_TYPE"] = $arSalon["PROPERTY_DOCTOR_TYPE_VALUE"];

		if(array_key_exists("PROPERTY_LICENSE_VALUE", $arSalon))
			$arResultSalon["LICENSE"] = $arSalon["PROPERTY_LICENSE_VALUE"];


		return $arResultSalon;
	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function GetByID ($intId)
	{
		$arSalon = self::GetList(array("=ID" => $intId));

		if(Util::IsNotEmptyArray($arSalon))
			return reset($arSalon);
		else
			return false;
	}

    /**
     * @param array $arSort
     * @return array
     */
	public static function GetAll (array $arSort = array())
	{
		return self::GetList(array("ACTIVE" => "Y"), $arSort);
	}


	public function Add($arFields)
	{

	}

	public function Update($id, $arFields)
	{

	}
}