<?
namespace Itgro\Repository;

use Itgro\Util;

class Appointment
{
	public static function GetList(array $arFilter = array(), array $arSort = array(), $arParams = false)
	{
		\CModule::IncludeModule("iblock");

		$arAppointments = array();

		$arDefaultFilter = array(
			"IBLOCK_ID" => \COption::GetOptionInt("itgro.doctor_entry", "appointment.iblock_id")
		);

		$arFilter = array_merge($arFilter, $arDefaultFilter);

		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"NAME",
			"PROPERTY_USER",
			"PROPERTY_REFINEMENTS",
			"PROPERTY_SALON",
			"PROPERTY_DOCTOR_TYPE",
			"PROPERTY_DATE",
			"PROPERTY_DURATION",
			"PROPERTY_NAME_OF_PATIENT",
			"PROPERTY_PHONE",
			"PROPERTY_EMAIL",
			"PROPERTY_COMMENT",
			"PROPERTY_DOCTOR_NAME",
		);

		$rsAppointments = \CIBlockElement::GetList($arSort, $arFilter, false, $arParams, $arSelect);

		while($arAppointment = $rsAppointments->getNext())
		{
			$arAppointments[] = self::FetchModifier($arAppointment);
		}

		return $arAppointments;
	}

	private static function FetchModifier($arAppointment)
	{
		$arResultAppointment = array();

		$arResultAppointment["ID"] = $arAppointment["ID"];
		$arResultAppointment["IBLOCK_ID"] = $arAppointment["IBLOCK_ID"];
		$arResultAppointment["NAME"] = $arAppointment["NAME"];

		if(array_key_exists("PROPERTY_USER_VALUE", $arAppointment))
			$arResultAppointment["USER"] = $arAppointment["PROPERTY_USER_VALUE"];

		if(array_key_exists("PROPERTY_REFINEMENTS_VALUE", $arAppointment))
			$arResultAppointment["REFINEMENTS"] = $arAppointment["PROPERTY_REFINEMENTS_VALUE"];

		if(array_key_exists("PROPERTY_SALON_VALUE", $arAppointment))
			$arResultAppointment["SALON"] = $arAppointment["PROPERTY_SALON_VALUE"];

		if(array_key_exists("PROPERTY_DOCTOR_TYPE_VALUE", $arAppointment))
			$arResultAppointment["DOCTOR_TYPE"] = $arAppointment["PROPERTY_DOCTOR_TYPE_VALUE"];

		if(array_key_exists("PROPERTY_DATE_VALUE", $arAppointment))
			$arResultAppointment["DATE"] = $arAppointment["PROPERTY_DATE_VALUE"];

		if(array_key_exists("PROPERTY_DURATION_VALUE", $arAppointment))
			$arResultAppointment["DURATION"] = $arAppointment["PROPERTY_DURATION_VALUE"];

		if(array_key_exists("PROPERTY_NAME_OF_PATIENT_VALUE", $arAppointment))
			$arResultAppointment["NAME_OF_PATIENT"] = $arAppointment["PROPERTY_NAME_OF_PATIENT_VALUE"];

		if(array_key_exists("PROPERTY_PHONE_VALUE", $arAppointment))
			$arResultAppointment["PHONE"] = $arAppointment["PROPERTY_PHONE_VALUE"];

		if(array_key_exists("PROPERTY_EMAIL_VALUE", $arAppointment))
			$arResultAppointment["EMAIL"] = $arAppointment["PROPERTY_EMAIL_VALUE"];

		if(array_key_exists("PROPERTY_COMMENT_VALUE", $arAppointment))
			$arResultAppointment["COMMENT"] = $arAppointment["PROPERTY_COMMENT_VALUE"];

		if(array_key_exists("PROPERTY_DOCTOR_NAME_VALUE", $arAppointment))
			$arResultAppointment["DOCTOR_NAME"] = $arAppointment["PROPERTY_DOCTOR_NAME_VALUE"];

		return $arResultAppointment;

	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function GetByID ($intId)
	{
		$arAppointment = self::GetList(array("=ID" => $intId));

		if(Util::IsNotEmptyArray($arAppointment))
			return reset($arAppointment);
		else
			return false;
	}

    /**
     * @param array $arSort
     * @return array
     */
	public static function GetAll (array $arSort = array())
	{
		return self::GetList(array("ACTIVE" => "Y"), $arSort);
	}

}