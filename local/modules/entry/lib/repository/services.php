<?
namespace Itgro\Repository;

use Itgro\Util;
use Itgro\Repository\Questions;
use Itgro\Repository\Answers;

class Services
{
	/**
	 * @param array $arFilter
	 * @param array $arSort
	 * @param array|bool $arParams
	 *
	 * @return array
	 */
	public static function GetList (array $arFilter = array(), array $arSort = array(), $arParams = false)
	{
		\CModule::IncludeModule("iblock");

		$arServices = array();

		$arDefaultFilter = array(
			"IBLOCK_ID" => \COption::GetOptionInt("itgro.doctor_entry", "services.iblock_id")
		);

		$arFilter = array_merge($arFilter, $arDefaultFilter);

		$rsServices = \CIBlockElement::GetList(
			$arSort, $arFilter, false, $arParams,
			array(
				"ID",
				"IBLOCK_ID",
				"NAME",
				"PROPERTY_XML_ID",
				"PROPERTY_LICENSE",
				"PROPERTY_COST",
				"PROPERTY_DURATION",
				"PROPERTY_DOCTOR_TYPE",
				"DETAIL_TEXT"
			)
		);

		while($arService = $rsServices->getNext(false, false))
		{
			$arService = self::FetchModifier($arService);

			$arServices[] = $arService;
		}

		return $arServices;
	}

	private static function FetchModifier($arService)
	{
		$arResultService = array();

		$arListAnswers = array(
			"Да" => "Y",
			"Нет" => "N"
		);

		$arResultService["ID"] = $arService["ID"];
		$arResultService["IBLOCK_ID"] = $arService["IBLOCK_ID"];
		$arResultService["NAME"] = $arService["NAME"];
		$arResultService["DETAIL_TEXT"] = $arService["DETAIL_TEXT"];

		if(array_key_exists("PROPERTY_XML_ID_VALUE", $arService))
			$arResultService["XML_ID"] = $arService["PROPERTY_XML_ID_VALUE"];

		if(array_key_exists("PROPERTY_LICENSE_VALUE", $arService) && array_key_exists($arService["PROPERTY_LICENSE_VALUE"], $arListAnswers))
			$arResultService["LICENSE"] = $arListAnswers[$arService["PROPERTY_LICENSE_VALUE"]];
		else
			$arResultService["LICENSE"] = false;

		if(array_key_exists("PROPERTY_COST_VALUE", $arService))
			$arResultService["COST"] = $arService["PROPERTY_COST_VALUE"];

		if(array_key_exists("PROPERTY_DURATION_VALUE", $arService))
			$arResultService["DURATION"] = $arService["PROPERTY_DURATION_VALUE"];

		if(array_key_exists("PROPERTY_DOCTOR_TYPE_VALUE", $arService))
			$arResultService["DOCTOR_TYPE"] = $arService["PROPERTY_DOCTOR_TYPE_VALUE"];

		return $arResultService;
	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function GetByID ($intId)
	{
		$arService = self::GetList(array("=ID" => $intId));

		if(Util::IsNotEmptyArray($arService))
			return reset($arService);
		else
			return false;
	}

    /**
     * @param array $arSort
     * @return array
     */
	public static function GetAll (array $arSort = array())
	{
		return self::GetList(array("ACTIVE" => "Y"), $arSort);
	}


	public function Add($arFields)
	{

	}

	public function Update($id, $arFields)
	{

	}
}