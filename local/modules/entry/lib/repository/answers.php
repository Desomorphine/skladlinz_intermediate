<?
namespace Itgro\Repository;

use Itgro\Util;

class Answers
{
	/**
	 * @param array $arFilter
	 * @param array $arSort
	 * @param array|bool $arParams
	 *
	 * @return array
	 */
	public static function GetList (array $arFilter = array(), array $arSort = array(), $arParams = false)
	{
		$arAnswers = array();

		$arDefaultFilter = array(
			"IBLOCK_ID" => \COption::GetOptionInt("itgro.doctor_entry", "answers.iblock_id")
		);

		$arFilter = array_merge($arFilter, $arDefaultFilter);

		$rsAnswers = \CIBlockElement::GetList(
			$arSort, $arFilter, false, $arParams,
			array(
				"ID",
				"IBLOCK_ID",
				"NAME",
				"PROPERTY_QUESTION",
				"PROPERTY_DOCTOR_TYPE",
				"PROPERTY_DURATION",
				"PROPERTY_COST",
				"PROPERTY_IS_HELD",
				"PROPERTY_LICENSE",
				"PROPERTY_CLARIFICATION",
			)
		);

		while($arAnswer = $rsAnswers->getNext(false, false))
		{
			$arAnswer = self::FetchModifier($arAnswer);

			$arAnswers[] = $arAnswer;
		}

		return $arAnswers;
	}

	private static function FetchModifier($arAnswer)
	{
		$arResultAnswer = array();

		$arListAnswers = array(
			"Да" => "Y",
			"Нет" => "N"
		);

		$arResultAnswer["ID"] = $arAnswer["ID"];
		$arResultAnswer["IBLOCK_ID"] = $arAnswer["IBLOCK_ID"];
		$arResultAnswer["NAME"] = $arAnswer["NAME"];

		if(array_key_exists("PROPERTY_QUESTION_VALUE", $arAnswer))
			$arResultAnswer["QUESTION"] = $arAnswer["PROPERTY_QUESTION_VALUE"];

		if(array_key_exists("PROPERTY_DOCTOR_TYPE_VALUE", $arAnswer))
			$arResultAnswer["DOCTOR_TYPE"] = $arAnswer["PROPERTY_DOCTOR_TYPE_VALUE"];

		if(array_key_exists("PROPERTY_DURATION_VALUE", $arAnswer))
			$arResultAnswer["DURATION"] = $arAnswer["PROPERTY_DURATION_VALUE"];

		if(array_key_exists("PROPERTY_COST_VALUE", $arAnswer))
			$arResultAnswer["COST"] = $arAnswer["PROPERTY_COST_VALUE"];

		if(array_key_exists("PROPERTY_IS_HELD_VALUE", $arAnswer) && array_key_exists($arAnswer["PROPERTY_IS_HELD_VALUE"], $arListAnswers))
			$arResultAnswer["IS_HELD"] = $arListAnswers[$arAnswer["PROPERTY_IS_HELD_VALUE"]];
		else
			$arResultAnswer["IS_HELD"] = false;

		if(array_key_exists("PROPERTY_LICENSE_VALUE", $arAnswer) && array_key_exists($arAnswer["PROPERTY_LICENSE_VALUE"], $arAnswer))
			$arResultAnswer["LICENSE"] = $arListAnswers[$arAnswer["PROPERTY_LICENSE_VALUE"]];
		else
			$arResultAnswer["LICENSE"] = false;

		if(array_key_exists("PROPERTY_CLARIFICATION_VALUE", $arAnswer))
			$arResultAnswer["CLARIFICATION"] = $arAnswer["PROPERTY_CLARIFICATION_VALUE"];


		return $arResultAnswer;
	}

	/**
	 * @param int $intId
	 * @return array
	 */
	public static function GetByID ($intId)
	{
		$arAnswer = self::GetList(array("=ID" => $intId));

		if(Util::IsNotEmptyArray($arAnswer))
			return reset($arAnswer);
		else
			return false;
	}

    /**
     * @param array $arSort
     * @return array
     */
	public static function GetAll (array $arSort = array())
	{
		return self::GetList(array("ACTIVE" => "Y"), $arSort);
	}


	public function Add($arFields)
	{

	}

	public function Update($id, $arFields)
	{

	}
}