<?
namespace Itgro;

class Util
{
	const YES = 1;
	const NO = 2;

	public static function Dump($var = "", $var_dump = null)
	{
		if (defined("ITGRO_DEBUGGER") && ITGRO_DEBUGGER == "Y")
		{
			echo "<pre>";

			$backtrace = debug_backtrace();

			echo '<h3>'.$backtrace[0]["file"].', '.$backtrace[0]["line"].'</h3>';

			$bNeedVarDump = !!$var_dump;

			if (is_null($var_dump))
			{
				if (is_array($var))
				{
					array_walk_recursive($var, function ($value) use (&$bNeedVarDump)
					{
						if (is_object($value) || is_bool($value) || is_null($value))
						{
							$bNeedVarDump = true;
						}
					});
				}
				elseif (is_string($var) && strlen($var) === 0)
				{
					$bNeedVarDump = true;
				}
			}

			if ($bNeedVarDump || is_object($var) || is_bool($var) || is_null($var))
			{
				var_dump($var);
			}
			elseif (is_array($var))
			{
				print_r($var);
			}
			else
			{
				echo $var;
			}

			echo "</pre>";
		}
	}

	public static function IsNotEmpty($var)
	{
		return self::IsNotEmptyArray($var) || self::IsIntegerAboveZero($var) || self::IsNotEmptyString($var);
	}

	public static function IsNotEmptyArray($var)
	{
		// TODO (itgro): Добавить возможность указывать callback-валидатор
		return (is_array($var) && !empty($var) && count($var) > 0);
	}

	public static function IsNotEmptyString($var)
	{
		// TODO (itgro): Добавить возможность указывать callback-валидатор
		return (is_string($var) && strlen($var) > 0);
	}

	public static function IsInteger($var)
	{
		return (string)intval($var) === (string)$var;
	}

	public static function IsIntegerAboveZero($var)
	{
		return self::IsInteger($var) && intval($var) > 0;
	}

	public static function ResizeImage($file, $arSize, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL, $bInitSizes = false, $arFilters = false, $bImmediate = false, $jpgQuality = false)
	{
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		return \CFile::ResizeImageGet($file, $arSize, $resizeType, $bInitSizes, $arFilters, $bImmediate, $jpgQuality);
	}

	/**
	 * Функция форматирования даты.
	 * Если возможно, возвращает дату в формате "01 янв. 2000 23:59".
	 * Если не удается установить русскую локаль, возвращает дату в формате "01.01.2000 23:59".
	 * Можно указать, чтобы возвращалась только дата, без времени.
	 * Если невозможно сформировать дату из переданного аргумента $date, возвращает значение, переданное в $placeholder.
	 *
	 * @param \DateTime $date
	 * @param bool $showTime
	 * @param string $placeholder
	 * @return string
	 */
	public static function FormatDate($date, $showTime = true, $placeholder = "")
	{
		if (is_object($date) && $date instanceof \DateTime)
		{
			if (setlocale(LC_ALL, "ru_RU.UTF-8"))
			{
				if ($showTime)
				{
					return strftime("%d %b %Y г. %H:%M", $date->getTimestamp());
				}
				else
				{
					return strftime("%d %b %Y г.", $date->getTimestamp());
				}
			}
			else
			{
				if ($showTime)
				{
					return $date->format("d.M.Y H:i");
				}
				else
				{
					return $date->format("d.M.Y");
				}
			}
		}
		else
		{
			return $placeholder;
		}
	}

	public static function FormatTime ($intTime, $strTimeType)
	{
		$strResult = "";
		switch($strTimeType)
		{
			case "H":
				$strResult = intval($intTime/(60*60));
				break;
			case "i":
				$strResult = intval(($intTime%(60*60))/60);
				break;
			case "s":
				$strResult = intval($intTime%60);
				break;
		}

		return (strlen($strResult) == 1)? "0".$strResult : $strResult;
	}

	public static function FormatFileSize ($size)
	{
		return str_replace(" ", "&nbsp;", \CFile::FormatSize($size, 0));
	}


	public static function GetUserName($user = null)
	{
		$obUser = new \CUser();
		$obSite = new \CSite();

		if (is_null($user))
		{
			$user = $obUser->GetID();
		}

		if (self::IsIntegerAboveZero($user))
		{
			$dbUser = $obUser->GetList(
				$by = "ID",
				$order = "ASC",
				array("ID_EQUAL_EXACT" => $user),
				array(
					"SELECT" => array(),
					"NAV_PARAMS" => array("nTopCount" => 1),
					"FIELDS" => array("ID", "NAME", "LAST_NAME", "SECOND_NAME", "LOGIN", "EMAIL"),
				)
			);

			$arUser = $dbUser->GetNext(true, false);

			if (!$arUser)
			{
				return "";
			}
		}
		elseif (self::IsNotEmptyArray($user))
		{
			$arUser = $user;
		}
		else
		{
			throw new \InvalidArgumentException(__METHOD__.": метод принимает в качестве аргумента ID пользователя или массив с его данными");
		}

		return \CUser::FormatName($obSite->GetNameFormat(false), $arUser, true, true);
	}

    public static function getMonthName($month)
    {
        $months = self::getNamesOfMonths();

        if(array_key_exists(intval($month), $months))
            return $months[intval($month)];

        return "";
    }

    public static function getNamesOfMonths()
    {
        return array(
            1 => "Январь", 2 => "Февраль",
            3 => "Март", 4 => "Апрель",
            5 => "Май", 6 => "Июнь",
            7 => "Июль", 8 => "Август",
            9 => "Сентябрь", 10 => "Октябрь",
            11 => "Ноябрь", 12 => "Декабрь",
        );
    }


	/**
	 * Функция чтобы показывать ошибки в модальном окне при открытии страницы.
	 * Можно передать в первый аргумент текстовую переменную, либо двумерный массив.
	 * Во втором случае обязательно укажите второй аргумент - ключ подмассива, в котором лежит текст ошибки.
	 *
	 * @param $errorsData
	 * @param null $strArrayKey
	 * @return bool
	 */
	public static function printErrors($errorsData, $strArrayKey = null)
	{
		$strErrorTxt = "";

		if(strlen($errorsData) > 0)
		{
			$strErrorTxt = $errorsData;
		}
		else if (self::IsNotEmptyArray($errorsData))
		{
			if ($strArrayKey === null)
				return false;

			foreach ($errorsData as $arError)
			{
				if(!array_key_exists($strArrayKey, $arError) || self::IsNotEmptyArray($arError[$strArrayKey]) || strlen($arError[$strArrayKey]) == 0)
					return false;

				$strErrorTxt .= $arError[$strArrayKey] . " ";
			}
		}
		else
		{
			return false;
		}

		echo "<div class=\"overlay js-modal-close\"  style=\"padding-top:20%\">
					<div class=\"modal js-modal-show-onload\">
				        <div class=\"modal__close js-modal-close\">&#215;</div>
				        <section class=\"content\">
					        <div class=\"content__title\">Во время выполнения произошла ошибка</div>
					        <div style=\"padding:10px\">" . $strErrorTxt . "</div>
				        </section>
				    </div>
			    </div>";

		return true;
	}

	public static function FormatPhone($strPhone)
	{
		if(strpos($strPhone, "+7") === 0)
			$strPhone = "8" . mb_substr($strPhone, 2);

		return $strPhone;
	}

	public static function clearExtraData(&$array, $arNecessaryFields)
	{
		foreach ($array as $field => $value)
		{
			if(!in_array($field, $arNecessaryFields))
				unset($array[$field]);
		}
		return $array;
	}
}
