<?
global $APPLICATION;
$mid = $_GET['mid'];
$module_id = "currency";
$CAT_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($CAT_RIGHT >= "R")
{
	CModule::IncludeModule("itgro.doctor_entry");

	$aTabs = array(
		array(
			"DIV" => "edit1",
			"TAB" => "Главные",
			"ICON" => "",
			"TITLE" => "Основные настройки"
		),
	);

	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	?>

	<? $tabControl->Begin(); ?>

	<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($mid) ?>&lang=<? echo LANGUAGE_ID ?>" name="ara">
		<? echo bitrix_sessid_post();

		$tabControl->BeginNextTab();

		$tabControl->EndTab();

		$tabControl->Buttons();
		?>

		<input type="submit" class="adm-btn-save" name="Update" value="<? echo GetMessage("MAIN_APPLY") ?>">
		<input type="hidden" name="Update" value="Y">

		<input type="reset" name="reset" value="<? echo GetMessage("MAIN_RESET") ?>">

		<? $tabControl->End(); ?>
	</form>
<? } ?>
