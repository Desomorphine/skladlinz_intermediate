<?
	
	$result = [];

	foreach (range(10, 21) as $h)
	{
		for ($m = 0; $m <= 45; $m = $m+15)
		{
			if ($m === 0) 
			{
				$m = '00';
			}

			$res['from'] = $h . ":" . $m;

			if ($m <= 30)
			{
				$res['to'] = $h . ":" . ($m + 15);
			}
			else
			{
				$res['to'] = ($h + 1) . ":" . '00';
			}
			
			$res['disable'] = (bool) rand(0,1);
			$res['active'] = false;

			$result[] = $res;
		}
	}

	$result = json_encode($result);

	echo $result;
?>