<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>
	<div id="wrap">
		<div id="content">
			<div id="left"></div>
			<div id="sort">
				<div id="pathway"><a href="/" title="Главная">Главная</a><a href="#0" title="Каталог">Персональный раздел</a>Запись к врачу</div>
				<h1>Записи ко врачу</h1>
				<div class="entry-doctor">
					<table>
						<tr>
							<th>Дата / Время</th>
							<th>Место приема</th>
							<th>Услуга</th>
							<th>Тип врача</th>
							<th class="doctor">ФИО врача</th>
							<th>Действия</th>
						</tr>
						<tr>
							<td>13.11.2014 15:30</td>
							<td>ул Металлургов, 87 (Мега)</td>
							<td>Подбор очков</td>
							<td>Детский</td>
							<td>Виктория Александровна Шатилова</td>
							<td><a href="#0" class="cancel js-modal" data-modal="#cancel">Отменить прием</a></td>
						</tr>
						<tr>
							<td>13.11.2014 15:30</td>
							<td>ул Металлургов, 87 (Мега)</td>
							<td>Подбор очков</td>
							<td>Детский</td>
							<td>Виктория Александровна Шатилова</td>
							<td><a href="#0" class="reload">Повторить запись</a></td>
						</tr>
					</table>					
				</div>
			</div>
		</div>	
	</div>
	<div class="imodal imodal--system" id="cancel">
		<div class="imodal__box">
			<div class="imodal__close js-imodal-close"></div>
			<div class="imodal__title">
				Отменить прием
			</div>
			<div class="message-error"></div>
			<div class="imodal__body">
				Вы уверенны, что хотите отменить прием?
			</div>
			<div class="imodal__footer imodal__footer--justify">
				<a href="#0" class="btn btn--primary btn--small">Да, я хочу отказаться</a>
				<a href="#0" class="btn btn--small">Перезаписаться</a>
				<a href="#0" class="btn btn--small">Нет, оставить запись</a>
			</div>
		</div>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/assets/vendor/jquery-v1.11.1.min.js"><\/script>')</script>
	<script src="/assets/js/main.js"></script>
</body>
</html>