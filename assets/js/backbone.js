/**
 * Класс для геокодирования списка адресов или координат.
 * @class
 * @name MultiGeocoder
 * @param {Object} [options={}] Дефолтные опции мультигеокодера.
 */
function MultiGeocoder(options) {
	this._options = options || {};
}

/**
 * Функция множественнеого геокодирования.
 * @function
 * @requires ymaps.util.extend
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.extend.xml
 * @requires ymaps.util.Promise
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.Promise.xml
 * @name MultiGeocoder.geocode
 * @param {Array} requests Массив строк-имен топонимов и/или геометрий точек (обратное геокодирование)
 * @returns {Object} Как и в обычном геокодере, вернем объект-обещание.
 */
MultiGeocoder.prototype.geocode = function (requests, options) {
	var self = this,
		size = requests.length,
		promise = new ymaps.util.Promise(),
		geoObjects = new ListCollection();

	requests.forEach(function (request, index) {
		ymaps.geocode(request, ymaps.util.extend({}, self._options, options))
			.then(
				function (response) {
					var geoObject = response.geoObjects.get(0);

					geoObject && geoObjects.add(geoObject, index);
					--size || promise.resolve({ geoObjects : geoObjects });
				},
				function (err) {
					promise.reject(err);
				}
			);
	});

	return promise;
};

function ListCollection() {
	ListCollection.superclass.constructor.apply(this, arguments);
	this._list = [];
}

ymaps.ready(function () {
	ymaps.util.augment(ListCollection, ymaps.GeoObjectCollection, {
		get: function (index) {
			return this._list[index];
		},
		add: function (child, index) {
			this.constructor.superclass.add.call(this, child);

			index = index == null ? this._list.length : index;
			this._list[index] = child;

			return this;
		},
		indexOf: function (o) {
			for(var i = 0, len = this._list.length; i < len; i++) {
				if(this._list[i] === o) {
					return i;
				}
			}

			return -1;
		},
		splice: function (index, number) {
			var added = Array.prototype.slice.call(arguments, 2),
				removed = this._list.splice.apply(this._list, arguments);

			for(var i = 0, len = added.length; i < len; i++) {
				this.add(added[i]);
			}

			for(var i = 0, len = removed.length; i < len; i++) {
				this.remove(removed[i]);
			}

			return removed;
		},
		remove: function (child) {
			this.constructor.superclass.remove.call(this, child);

			// this._list.splice(this.indexOf(child), 1);
			delete this._list[this.indexOf(child)];

			return this;
		},
		removeAll: function () {
			this.constructor.superclass.removeAll.call(this);

			this._list = [];

			return this;
		},
		each: function (callback, context) {
			for(var i = 0, len = this._list.length; i < len; i++) {
				callback.call(context, this._list[i]);
			}
		}
	});
});

function map(response) {
	var MyPlacemark,
		myGeocoder,
		address = [];

	ymaps.ready(init);

	function init() {
		_.each(response.models, function(salon, i) {
			address[i] = salon.get('address')
		})

		App.myMap =  new ymaps.Map('map', {
			center: [56.836, 60.600],
			zoom: 13,
			behaviors: ['default', 'scrollZoom']
		});

		App.myMap.controls.add('mapTools');

		myGeocoder = new MultiGeocoder({boundedBy : App.myMap.getBounds()});

		// Геокодирование массива адресов и координат.
		myGeocoder.geocode(address)
		.then(function (res) {
				// Асинхронно получаем коллекцию найденных геообъектов.
				App.myMap.geoObjects.add(res.geoObjects);

				var list = res.geoObjects._list
				for (var i = list.length - 1; i >=0; i--) {
					response.at(i).set('coordinate', list[i].geometry._Mb);     
				}
				$('.js-salon-select').trigger('change');     
			},
			function (err) {
				console.log(err);
			});
	}
}
/**
 * Backbone localStorage Adapter
 * Version 1.1.16
 *
 * https://github.com/jeromegn/Backbone.localStorage
 */(function(a,b){typeof exports=="object"&&typeof require=="function"?module.exports=b(require("backbone")):typeof define=="function"&&define.amd?define(["backbone"],function(c){return b(c||a.Backbone)}):b(Backbone)})(this,function(a){function b(){return((1+Math.random())*65536|0).toString(16).substring(1)}function c(){return b()+b()+"-"+b()+"-"+b()+"-"+b()+"-"+b()+b()+b()}function d(a){return a===Object(a)}function e(a,b){var c=a.length;while(c--)if(a[c]===b)return!0;return!1}function f(a,b){for(var c in b)a[c]=b[c];return a}function g(a,b){if(a==null)return void 0;var c=a[b];return typeof c=="function"?a[b]():c}return a.LocalStorage=window.Store=function(a,b){if(!this.localStorage)throw"Backbone.localStorage: Environment does not support localStorage.";this.name=a,this.serializer=b||{serialize:function(a){return d(a)?JSON.stringify(a):a},deserialize:function(a){return a&&JSON.parse(a)}};var c=this.localStorage().getItem(this.name);this.records=c&&c.split(",")||[]},f(a.LocalStorage.prototype,{save:function(){this.localStorage().setItem(this.name,this.records.join(","))},create:function(a){return!a.id&&a.id!==0&&(a.id=c(),a.set(a.idAttribute,a.id)),this.localStorage().setItem(this._itemName(a.id),this.serializer.serialize(a)),this.records.push(a.id.toString()),this.save(),this.find(a)},update:function(a){this.localStorage().setItem(this._itemName(a.id),this.serializer.serialize(a));var b=a.id.toString();return e(this.records,b)||(this.records.push(b),this.save()),this.find(a)},find:function(a){return this.serializer.deserialize(this.localStorage().getItem(this._itemName(a.id)))},findAll:function(){var a=[];for(var b=0,c,d;b<this.records.length;b++)c=this.records[b],d=this.serializer.deserialize(this.localStorage().getItem(this._itemName(c))),d!=null&&a.push(d);return a},destroy:function(a){this.localStorage().removeItem(this._itemName(a.id));var b=a.id.toString();for(var c=0,d;c<this.records.length;c++)this.records[c]===b&&this.records.splice(c,1);return this.save(),a},localStorage:function(){return localStorage},_clear:function(){var a=this.localStorage(),b=new RegExp("^"+this.name+"-");a.removeItem(this.name);for(var c in a)b.test(c)&&a.removeItem(c);this.records.length=0},_storageSize:function(){return this.localStorage().length},_itemName:function(a){return this.name+"-"+a}}),a.LocalStorage.sync=window.Store.sync=a.localSync=function(b,c,d){var e=g(c,"localStorage")||g(c.collection,"localStorage"),f,h,i=a.$?a.$.Deferred&&a.$.Deferred():a.Deferred&&a.Deferred();try{switch(b){case"read":f=c.id!=undefined?e.find(c):e.findAll();break;case"create":f=e.create(c);break;case"update":f=e.update(c);break;case"delete":f=e.destroy(c)}}catch(j){j.code===22&&e._storageSize()===0?h="Private browsing is unsupported":h=j.message}return f?(d&&d.success&&(a.VERSION==="0.9.10"?d.success(c,f,d):d.success(f)),i&&i.resolve(f)):(h=h?h:"Record Not Found",d&&d.error&&(a.VERSION==="0.9.10"?d.error(c,h,d):d.error(h)),i&&i.reject(h)),d&&d.complete&&d.complete(f),i&&i.promise()},a.ajaxSync=a.sync,a.getSyncMethod=function(b,c){var d=c&&c.ajaxSync;return!d&&(g(b,"localStorage")||g(b.collection,"localStorage"))?a.localSync:a.ajaxSync},a.sync=function(b,c,d){return a.getSyncMethod(c,d).apply(this,[b,c,d])},a.LocalStorage});
window.App = {
	Models: {},
	Collections: {},
	Views: {},
	Router: {},
}

window.template = function(id) {
	return _.template($('#'+id).html());
}

$(document).ajaxStart(function() { 
	$('.js-preloader').addClass('preloader');
});

$(document).ajaxStop(function() { 
	$('.js-preloader').removeClass('preloader');
});

//EVENTS
App.vent = {};
_.extend(App.vent, Backbone.Events);

/*$.ajaxPrefilter(function(options, originalOptions, jqXHR) {
	options.url = '/module.php' + options.url;
});*/

var href = window.location.hash || '#service';

$('.js-modal').attr('href', href)
App.Models.Services = Backbone.Model.extend({
	defaults: {},
	urlRoot: '/module/service/',
	initialize: function() {
		this.services = new App.Collections.Services();
		this.questions = {};
	}
});

App.Models.Service = Backbone.Model.extend({
	defaults: {},
});

App.Models.Question = Backbone.Model.extend({
	defaults: {
		answer_check: '',
	},
});

App.Models.Menu = Backbone.Model.extend({
	defaults: {
		id: '',
	},
})

App.Models.Salons = Backbone.Model.extend({
	urlRoot: '/module/salon/',
	initialize: function() {
		this.is_held = 'n';
		this.salons = '';
	}
});

App.Models.Salon = Backbone.Model.extend({
	defaults: {
		coordinate: '',
	}
});

App.Models.Day = Backbone.Model.extend({
	defaults: {
		date: '',
		day: '',
	},
})

App.Models.Time = Backbone.Model.extend({
	defaults: {},
});

App.Models.Timetable = Backbone.Model.extend({
	defaults: {
		date: '',
		time: ''
	},
	urlRoot: '/module/timetable/',
});

App.Models.Comments = Backbone.Model.extend({
	defaults: {},
	urlRoot: '/module/comments',
	validate: function(attrs) {
		var i = null;
		for (var attr in attrs) {
			var $input = $('[name='+attr+']');

			if ($input.hasClass('js-required') && $input.val().length === 0) {
				$input.addClass('error');
				i++;
			} else {
				$input.removeClass('error');
			}
		}

		if (i > 0) {
			$('.message-error').html('Заполните, пожалуйста, обязательные поля.').show();
			return 'error';
		} else {
			$('.message-error').hide();
		}
	}	
});

App.Models.Result = Backbone.Model.extend({
	defaults: {
		result: '',
	},
	urlRoot: '/module/result/'
});

App.Models.ClientInfo = Backbone.Model.extend({
	defaults: {
		services: '',
		cost: '',
		doctor: '',
		duration: '',
		salon: '',
	},
	initialize: function() {
		if (localStorage.clientInfo) {
			var store = JSON.parse(localStorage['clientInfo-'+localStorage.clientInfo]),
				self = this;

			_.each(self.attributes, function(attr, i) {
				self.set(i, store[i]);
			})
		} else {
			this.localStorage = new Backbone.LocalStorage('clientInfo');
		}
	},
});

App.result = new App.Models.Result();
App.timetable = new App.Models.Timetable();
App.clientInfo = new App.Models.ClientInfo();
App.Collections.Menu = Backbone.Collection.extend({
	model: App.Models.Menu,
});

App.Collections.Services =  Backbone.Collection.extend({
	model: App.Models.Service,
	url: '/module/service/',
})

App.Collections.Questions =  Backbone.Collection.extend({
	model: App.Models.Question,
	url: '/module/service/',
});

App.Collections.Salons = Backbone.Collection.extend({
	model: App.Models.Salon,
});

App.Collections.Days = Backbone.Collection.extend({
	model: App.Models.Day,
	initialize: function() {
		var today = new Date(),
			to = 60, //2 месяца
			week = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
			days = [],
			d = today;

		for (var i = 0; to > i; i++) {
			d = new Date(today.getTime() + 86400000*i);

			days[i] = {};
			days[i]['active'] = false;
			days[i]['date'] = getDate(d);
			days[i]['day'] = week[d.getDay()];
		};

		function getZero(i) {
			i = i < 10 ? '0' + i : i;
			return i;
		};

		function getDate(date) {
			var d = getZero(date.getDate()),
				m = getZero(date.getMonth() + 1),
				y = date.getFullYear();
			
			return d+'.'+m+'.'+y	
		};

		days[0].active = true;

		this.reset(days);
	}
});

App.Collections.Time = Backbone.Collection.extend({
	model: App.Models.Time,
	url: '/module/timetable/',
});

var menu = [
	{
		name    : 'Услуга',
		url     : '#service',
		control : {
			next : {
				name : 'Далее к выбору салона',
				url : 'salon'
			},			
		}	
	},
	{
		name    : 'Салон',
		url     : '#salon',
		control : {
			next : {
				name : 'Далее к времени визита',
				url : 'timetable'
			},
			prev : {
				name : 'Назад в выбору услуги',
				url : '#service'
			}
		}
	},
	{
		name    : 'Время визита',
		url     : '#timetable',
		control : {
			next : {
				name : 'Далее к контактам',
				url : 'comments'
			},
			prev : {
				name : 'Назад к выбору салона',
				url : '#salon'
			}
		}
	},
	{
		name    : 'Контакты и пожелания',
		url     : '#comments',
		control : {
			next : {
				name : 'Записаться',
				url : 'result'
			},
			prev : {
				name : 'Назад к времени визита',
				url : '#timetable'
			}
		}
	},
	{
		name   : 'Результат записи',
		url    : '#result',
		control: {}
	}
];

App.info = {};

App.menu = new App.Collections.Menu(menu);
App.days = new App.Collections.Days();
function send(opt) {
	var attrs = {},
		controls = [];

	controls.push(opt.view.$el.find('input.form__control'));
	controls.push(opt.view.$el.find('textarea.form__control'));
	controls.push(opt.view.$el.find('option:checked'));
	controls.push(opt.view.$el.find('input:checked'));

	function getName(el) {
		var n = el.getAttribute('name');
		if (n) {
			attrs[n] = el.value;
		} else {
			return getName(el.parentNode);
		}
	}

	_.each(controls, function(control) {
		_.each(control, function(el) {
			getName(el);
		})
	})

	opt.view.model.save(attrs, {
		success: function(collection, response) {
			if (response.status === 'ok') {
				$('.message-error').hide();
				window.location.hash = opt.nextUrl;
			} else if (response.status === 'error') {
				$('.message-error').html(response.message).show();
			}
			else {
				$('.message-error').html('Возникли проблемы при сохранении. Пожалуйста, повторите попытку.').show();
			}
		},
		error: function() {
			$('.message-error').html('Сервер временно недоступен. Пожалуйста, повторите попытку чуть позже.').show();
		}		
	});

	App.clientInfo.save();
}

App.Views.Menu = Backbone.View.extend({
	el: '.js-menu',
	template: template('menu'),
	render: function(route) {
		var hash = '#'+route;

		this.$el.html(this.template({active: hash, menu: this.collection.toJSON()}));

		App.viewMenuControl.render({
			route: route,
			control: this.collection.findWhere({url: hash}).toJSON().control
		});

		return this;
	},
});

App.Views.MenuControl = Backbone.View.extend({
	el: '#container',
	className: '.imodal__footer',
	template: template('menu-control'),
	render: function(options) {
		this.activeRoute = options.route;
		this.control = options.control;
		this.$el.find(this.className).html(this.template({control: options.control}));
	},
	events: {
		'submit': 'send'
	},
	send: function(e) {
		e.preventDefault();

		switch (this.activeRoute) {
			case 'service':
				send({
					view: App.viewServices,
					nextUrl: this.control.next.url,
				});
				break;
			case 'salon':
				send({
					view: App.viewSalons,
					nextUrl: this.control.next.url,
				});
				break;
			case 'timetable':
				send({
					view: App.viewTimetable,
					nextUrl: this.control.next.url,
				});
				break;
			case 'comments':
				send({
					view: App.viewComments,
					nextUrl: this.control.next.url,
				});
				break;
			case 'result':
				//App.viewResult.send();
		}
	}
});

App.Views.Services = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('services'),
	initialize: function() {
		var self = this,
			m = self.model;

		m.services.fetch({
			success: function(collection, response, options) {
				_.each(response, function(model) {
					m.questions[model.id] = new App.Collections.Questions;
					m.questions[model.id].url += model.id;
				})

				self.render();
			},
			error: function(collection, response, options) {
				console.log('error');
			}
		})
	},
	render: function() {
		this.$el.html(this.template({services: this.model.services.toJSON()}));

		//Инициализируем зависимые виды.
		App.viewQuestions = new App.Views.Questions();
		App.viewDuration = new App.Views.Duration();
		App.viewServiceLink = new App.Views.ServiceLink();

		this.$el.find('.js-select').chosen({
			disable_search_threshold: 1000,
			no_results_text: 'Не найдено',
		}).trigger('change');

		return this;
	},
	events: {
		'change .js-service-select': 'changed',
	},
	changed: function(el) {
		var id = $(el.target).find('option:checked').val(),
			detail = this.model.services.get(id).get('detail_link'); //detail service
			c = this.model.questions[id]; //Collection questions

		//Questions
		if (_.isEmpty(c.models)) {
			c.fetch({
				success: function(collection, response, options) {
					App.viewQuestions.render(collection);
				}
			})
		} else {
			App.viewQuestions.render(c);
		}

		//Duration
		App.duration = [];
		App.duration.push(this.model.services.get(id).get('duration'));
		App.viewDuration.render(App.duration);

		//Detail link service
		App.viewServiceLink.render(detail);

		App.clientInfo.set('services', this.model.services.get(id).get('name'));
		App.clientInfo.set('cost', this.model.services.get(id).get('cost'));
	},
})

App.Views.Questions = Backbone.View.extend({
	el: '#container .js-questions',
	render: function(collection) {
		var self = this;

		self.collection = collection;
		self.$el.html('');

		_.each(self.collection.models, function(m) {
			var question = new App.Views.Question({model: m});
			self.$el.append(question.render().el);
		})
	},
});

App.Views.Question = Backbone.View.extend({
	className: 'form__group',
	template: template('question'),
	render: function() {
		this.$el.html(this.template({question: this.model.toJSON()}));

		return this;
	},
	events: {
		'change': 'changed'
	},
	changed: function() {
		var id = this.$el.find('input:checked').val(),
			i = this.$el.index() + 1; //Первое значение - услуга, поэтому сдвигаемся на 1.
			doc  = this.model.toJSON().answers[id].doctor ? this.model.toJSON().answers[id].doctor : '';

		this.model.set('answer_check', id);

		App.duration[i] = this.model.toJSON().answers[id].duration;
		App.viewDuration.render(App.duration);

		App.clientInfo.set('doctor', doc); 
	}
});

App.Views.ServiceLink = Backbone.View.extend({
	className: '.js-detail-link',
	template: template('detail-link'),
	render: function(detail) {
		$(this.className).html(this.template({detailLink: detail}));
	}
});

App.Views.Duration = Backbone.View.extend({
	className: '.js-duration',
	template: template('duration'),
	render: function(arr) {
		var d = Math.max.apply(null, arr);

		$(this.className).html(this.template({duration: d}));

		App.clientInfo.set('duration', d);
	}
});

App.Views.Salons = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('salon'),
	templateDetail: template('salon-detail'),
	initialize: function() {
		var self = this;

		self.model.fetch({
			success: function(collection, response, options) {
				self.model.clear();
				if (response.is_held !== undefined) {
					self.model.is_held = response.is_held;
				}

				self.model.salons = new App.Collections.Salons(response.salons);
				self.render();
			},
			error: function() {
				console.log('error');
			}
		});
	},
	render: function() {
		this.$el.html(this.template({salons: this.model.salons.toJSON()}));

		$('.js-select').chosen({
			disable_search_threshold: 1000,
			no_results_text: 'Не найдено',
		});

		//Метки на карте
		map(this.model.salons);
	},
	events: {
		'change .js-salon-select' : 'changed'
	},
	changed: function(el) {
		var id = $(el.target).find('option:checked').val(),
			salon = this.model.salons.get(id),
			coordinate = this.model.salons.get(id).get('coordinate');

		this.$('.js-salon-detail').html(this.templateDetail({salon: salon.toJSON()}));

		App.myMap.setCenter(coordinate);
		App.clientInfo.set('salon', this.model.salons.get(id).get('address'));
	},
});

App.Views.SalonDetail = Backbone.View.extend({
	el: '.js-salon-detail',
	template: template('timetable'),
	render: function(salon) {
		this.$el.html(this.template({salon: salon.toJSON()}));
	}
});

App.Views.Timetable = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('timetable'),
	initialize: function() {
		this.render();
	},
	render: function() {
		console.log(App.clientInfo);
		this.$el.html(this.template({info: App.clientInfo.toJSON()}));

		App.viewTime = new App.Views.Time({collection: new App.Collections.Time()});
		App.viewCalendar = new App.Views.Calendar({collection: App.days});
	}
})

App.Views.Calendar = Backbone.View.extend({
	el: '.js-calendar',
	template: template('calendar'),
	inputDate: null,
	days: null,
	carousel: null,
	initialize: function() {
		this.render();
	},
	render: function() {
		var self= this;

		this.$el.html(this.template({calendar: this.collection.toJSON()}));
		this.inputDate = this.$el.find('.form__control--date');
		this.days = this.$el.find('.calendar__day');
		this.carousel = this.$el.find('.calendar__viewport');

		//Carousel
		this.carousel.jcarousel();
		$('.calendar__control--prev')
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				target: '-=6'
			});
		$('.calendar__control--next')
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				target: '+=6'
			});

		//Mask
		this.inputDate.inputmask('dd.mm.yyyy', {
			yearrange: { minyear: self.getDate().slice(6)},
			radixPoint: '.'
		});

		App.viewTime.render(this.getDate());
	},
	getDate: function() {
		return this.collection.findWhere({active: true}).get('date');
	},
	events: {
		'click .calendar__day' : 'changeDay',
		'change .form__control--date' : 'changeDay',
	},
	changeDay: function(e) {
		var $target = $(e.target),
			prevDate,
			dayActive,
			date;

		if (e.type === 'click') {
			date = this.collection.at($target.index());
		}

		if (e.type === 'change') {
			date = this.collection.findWhere({date: $target.val()});
		}

		if (date) {
			prevDate = this.collection.findWhere({active: true}).set({active: false});
			$(this.days[this.collection.indexOf(prevDate)]).removeClass('calendar__day--active')
			
			date.set({active : true});
			dayActive = $(this.days[this.collection.indexOf(date)]).addClass('calendar__day--active');
			this.inputDate.val(date.get('date'));
			App.viewTime.render(date.get('date'));

			//Перематываем карусель если выбранный день за пределами видимости
			if (! this.carousel.jcarousel('visible').filter(dayActive).length) {
				this.carousel.jcarousel('scroll', dayActive.index());
			}

			$('.message-error').hide();
		} else {
			$('.message-error').html('На введенную вами дату ('+ $target.val() +') - запись не доступна.').show();
			this.inputDate.val(this.getDate());
		}
	},
});

App.Views.Time = Backbone.View.extend({
	el: '.js-time',
	template: template('time'),
	inputTime: null,
	listTime: null,
	render: function(day) {
		var self = this;

		self.collection.url = '/time.php'; //TODO delete

		self.collection.fetch({
			data: {day: day},
			success: function(collection, response, options) {
				self.collection.findWhere({disable: false}).set({active: true});
				self.$el.html(self.template({time: self.collection.toJSON()}));

				self.inputTime = self.$el.find('.form__control--time');
				self.listTime = self.$el.find('.time__el');

				self.inputTime.inputmask('9{2}:9{2}');
			},
			error: function(collection, response, options) {
				console.log('error');
			}
		});
	},
	events: {
		'click .time__el:not(.time__el--disable)' : 'changeTime',
		'change .form__control--time' : 'changeTime'
	},
	getTime: function() {
		return this.collection.findWhere({active: true}).get('from');
	},
	changeTime: function(e) {
		var $target = $(e.target),
			time,
			prevTime;

		if (e.type === 'click') {
			time = this.collection.at($target.index());
		}

		if (e.type === 'change') {
			time = this.collection.findWhere({from : $target.val(), disable: false});
		}

		if (time) {
			prevTime = this.collection.findWhere({active: true}).set({active: false});
			$(this.listTime[this.collection.indexOf(prevTime)]).removeClass('time__el--active')
			
			time.set({active : true});
			$(this.listTime[this.collection.indexOf(time)]).addClass('time__el--active')
			this.inputTime.val(time.get('from'));
			$('.message-error').hide();
		} else {
			$('.message-error').html('На введенное вами время ('+ $target.val() +') - запись не доступна.').show();
			this.inputTime.val(this.getTime());
		}
	}
});

App.Views.Comments = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('comments'),
	initialize: function() {
		this.$el.html(this.template());
	},
	render: function() {
		this.$el.html(this.template());
	},
});

App.Views.Result = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('result'),
	render: function() {
		var self = this;

		self.model.fetch({
			success: function(model, response, options) {
				self.$el.html(self.template({result: model.toJSON()}));
			},
			error: function() {
				self.$el.html(self.template());
				console.log('error');
			}
		})
	},
});

//MENU
App.viewMenuControl = new App.Views.MenuControl();
App.viewMenu = new App.Views.Menu({collection: App.menu});

//RESULT
App.viewResult = new App.Views.Result({model: App.result});

App.Router = Backbone.Router.extend({
	routes: {
		'service'       : 'service',
		'salon'     	: 'salon',
		'timetable'     : 'timetable',
		'comments'      : 'comments',
		'result'        : 'result',
	},
	service: function() {
		if (App.viewServices === undefined) {
			App.viewServices = new App.Views.Services({model: new App.Models.Services()});
		} else {
			App.viewServices.render();
		}
	},
	salon: function() {
		if (App.viewSalon === undefined) {
			App.salons = new App.Models.Salons();
			App.viewSalons = new App.Views.Salons({model: App.salons});		
		} else {
			App.viewSalon.render();
		}	
	},
	timetable: function() {
		if (App.viewTimetable === undefined) {
			App.viewTimetable = new App.Views.Timetable({model: App.timetable});
		} else {
			App.viewTimetable.render();
		}
	},
	comments: function() {
		if (App.viewComments === undefined) {
			App.comments = new App.Models.Comments();
			App.viewComments = new App.Views.Comments({model: App.comments});
		} else {
			App.viewComments.render();
		}
	},
	result: function() {
		App.viewResult.render();
	},
})

App.route = new App.Router();
App.route.on('route', function(route) {
	App.viewMenu.render(route);
})
Backbone.history.start();