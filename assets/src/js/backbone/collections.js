App.Collections.Menu = Backbone.Collection.extend({
	model: App.Models.Menu,
});

App.Collections.Services =  Backbone.Collection.extend({
	model: App.Models.Service,
	url: '/module/service/',
})

App.Collections.Questions =  Backbone.Collection.extend({
	model: App.Models.Question,
	url: '/module/service/',
});

App.Collections.Salons = Backbone.Collection.extend({
	model: App.Models.Salon,
});

App.Collections.Days = Backbone.Collection.extend({
	model: App.Models.Day,
	initialize: function() {
		var today = new Date(),
			to = 60, //2 месяца
			week = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
			days = [],
			d = today;

		for (var i = 0; to > i; i++) {
			d = new Date(today.getTime() + 86400000*i);

			days[i] = {};
			days[i]['active'] = false;
			days[i]['date'] = getDate(d);
			days[i]['day'] = week[d.getDay()];
		};

		function getZero(i) {
			i = i < 10 ? '0' + i : i;
			return i;
		};

		function getDate(date) {
			var d = getZero(date.getDate()),
				m = getZero(date.getMonth() + 1),
				y = date.getFullYear();
			
			return d+'.'+m+'.'+y	
		};

		days[0].active = true;

		this.reset(days);
	}
});

App.Collections.Time = Backbone.Collection.extend({
	model: App.Models.Time,
	url: '/module/timetable/',
});

var menu = [
	{
		name    : 'Услуга',
		url     : '#service',
		control : {
			next : {
				name : 'Далее к выбору салона',
				url : 'salon'
			},			
		}	
	},
	{
		name    : 'Салон',
		url     : '#salon',
		control : {
			next : {
				name : 'Далее к времени визита',
				url : 'timetable'
			},
			prev : {
				name : 'Назад в выбору услуги',
				url : '#service'
			}
		}
	},
	{
		name    : 'Время визита',
		url     : '#timetable',
		control : {
			next : {
				name : 'Далее к контактам',
				url : 'comments'
			},
			prev : {
				name : 'Назад к выбору салона',
				url : '#salon'
			}
		}
	},
	{
		name    : 'Контакты и пожелания',
		url     : '#comments',
		control : {
			next : {
				name : 'Записаться',
				url : 'result'
			},
			prev : {
				name : 'Назад к времени визита',
				url : '#timetable'
			}
		}
	},
	{
		name   : 'Результат записи',
		url    : '#result',
		control: {}
	}
];

App.info = {};

App.menu = new App.Collections.Menu(menu);
App.days = new App.Collections.Days();