window.App = {
	Models: {},
	Collections: {},
	Views: {},
	Router: {},
}

window.template = function(id) {
	return _.template($('#'+id).html());
}

$(document).ajaxStart(function() { 
	$('.js-preloader').addClass('preloader');
});

$(document).ajaxStop(function() { 
	$('.js-preloader').removeClass('preloader');
});

//EVENTS
App.vent = {};
_.extend(App.vent, Backbone.Events);

/*$.ajaxPrefilter(function(options, originalOptions, jqXHR) {
	options.url = '/module.php' + options.url;
});*/

var href = window.location.hash || '#service';

$('.js-modal').attr('href', href)