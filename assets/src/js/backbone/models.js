App.Models.Services = Backbone.Model.extend({
	defaults: {},
	urlRoot: '/module/service/',
	initialize: function() {
		this.services = new App.Collections.Services();
		this.questions = {};
	}
});

App.Models.Service = Backbone.Model.extend({
	defaults: {},
});

App.Models.Question = Backbone.Model.extend({
	defaults: {
		answer_check: '',
	},
});

App.Models.Menu = Backbone.Model.extend({
	defaults: {
		id: '',
	},
})

App.Models.Salons = Backbone.Model.extend({
	urlRoot: '/module/salon/',
	initialize: function() {
		this.is_held = 'n';
		this.salons = '';
	}
});

App.Models.Salon = Backbone.Model.extend({
	defaults: {
		coordinate: '',
	}
});

App.Models.Day = Backbone.Model.extend({
	defaults: {
		date: '',
		day: '',
	},
})

App.Models.Time = Backbone.Model.extend({
	defaults: {},
});

App.Models.Timetable = Backbone.Model.extend({
	defaults: {
		date: '',
		time: ''
	},
	urlRoot: '/module/timetable/',
});

App.Models.Comments = Backbone.Model.extend({
	defaults: {},
	urlRoot: '/module/comments',
	validate: function(attrs) {
		var i = null;
		for (var attr in attrs) {
			var $input = $('[name='+attr+']');

			if ($input.hasClass('js-required') && $input.val().length === 0) {
				$input.addClass('error');
				i++;
			} else {
				$input.removeClass('error');
			}
		}

		if (i > 0) {
			$('.message-error').html('Заполните, пожалуйста, обязательные поля.').show();
			return 'error';
		} else {
			$('.message-error').hide();
		}
	}	
});

App.Models.Result = Backbone.Model.extend({
	defaults: {
		result: '',
	},
	urlRoot: '/module/result/'
});

App.Models.ClientInfo = Backbone.Model.extend({
	defaults: {
		services: '',
		cost: '',
		doctor: '',
		duration: '',
		salon: '',
	},
	initialize: function() {
		if (localStorage.clientInfo) {
			var store = JSON.parse(localStorage['clientInfo-'+localStorage.clientInfo]),
				self = this;

			_.each(self.attributes, function(attr, i) {
				self.set(i, store[i]);
			})
		} else {
			this.localStorage = new Backbone.LocalStorage('clientInfo');
		}
	},
});

App.result = new App.Models.Result();
App.timetable = new App.Models.Timetable();
App.clientInfo = new App.Models.ClientInfo();