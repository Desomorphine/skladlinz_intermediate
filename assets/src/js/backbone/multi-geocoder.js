/**
 * Класс для геокодирования списка адресов или координат.
 * @class
 * @name MultiGeocoder
 * @param {Object} [options={}] Дефолтные опции мультигеокодера.
 */
function MultiGeocoder(options) {
	this._options = options || {};
}

/**
 * Функция множественнеого геокодирования.
 * @function
 * @requires ymaps.util.extend
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.extend.xml
 * @requires ymaps.util.Promise
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.Promise.xml
 * @name MultiGeocoder.geocode
 * @param {Array} requests Массив строк-имен топонимов и/или геометрий точек (обратное геокодирование)
 * @returns {Object} Как и в обычном геокодере, вернем объект-обещание.
 */
MultiGeocoder.prototype.geocode = function (requests, options) {
	var self = this,
		size = requests.length,
		promise = new ymaps.util.Promise(),
		geoObjects = new ListCollection();

	requests.forEach(function (request, index) {
		ymaps.geocode(request, ymaps.util.extend({}, self._options, options))
			.then(
				function (response) {
					var geoObject = response.geoObjects.get(0);

					geoObject && geoObjects.add(geoObject, index);
					--size || promise.resolve({ geoObjects : geoObjects });
				},
				function (err) {
					promise.reject(err);
				}
			);
	});

	return promise;
};

function ListCollection() {
	ListCollection.superclass.constructor.apply(this, arguments);
	this._list = [];
}

ymaps.ready(function () {
	ymaps.util.augment(ListCollection, ymaps.GeoObjectCollection, {
		get: function (index) {
			return this._list[index];
		},
		add: function (child, index) {
			this.constructor.superclass.add.call(this, child);

			index = index == null ? this._list.length : index;
			this._list[index] = child;

			return this;
		},
		indexOf: function (o) {
			for(var i = 0, len = this._list.length; i < len; i++) {
				if(this._list[i] === o) {
					return i;
				}
			}

			return -1;
		},
		splice: function (index, number) {
			var added = Array.prototype.slice.call(arguments, 2),
				removed = this._list.splice.apply(this._list, arguments);

			for(var i = 0, len = added.length; i < len; i++) {
				this.add(added[i]);
			}

			for(var i = 0, len = removed.length; i < len; i++) {
				this.remove(removed[i]);
			}

			return removed;
		},
		remove: function (child) {
			this.constructor.superclass.remove.call(this, child);

			// this._list.splice(this.indexOf(child), 1);
			delete this._list[this.indexOf(child)];

			return this;
		},
		removeAll: function () {
			this.constructor.superclass.removeAll.call(this);

			this._list = [];

			return this;
		},
		each: function (callback, context) {
			for(var i = 0, len = this._list.length; i < len; i++) {
				callback.call(context, this._list[i]);
			}
		}
	});
});

function map(response) {
	var MyPlacemark,
		myGeocoder,
		address = [];

	ymaps.ready(init);

	function init() {
		_.each(response.models, function(salon, i) {
			address[i] = salon.get('address')
		})

		App.myMap =  new ymaps.Map('map', {
			center: [56.836, 60.600],
			zoom: 13,
			behaviors: ['default', 'scrollZoom']
		});

		App.myMap.controls.add('mapTools');

		myGeocoder = new MultiGeocoder({boundedBy : App.myMap.getBounds()});

		// Геокодирование массива адресов и координат.
		myGeocoder.geocode(address)
		.then(function (res) {
				// Асинхронно получаем коллекцию найденных геообъектов.
				App.myMap.geoObjects.add(res.geoObjects);

				var list = res.geoObjects._list
				for (var i = list.length - 1; i >=0; i--) {
					response.at(i).set('coordinate', list[i].geometry._Mb);     
				}
				$('.js-salon-select').trigger('change');     
			},
			function (err) {
				console.log(err);
			});
	}
}