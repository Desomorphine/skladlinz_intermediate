function send(opt) {
	var attrs = {},
		controls = [];

	controls.push(opt.view.$el.find('input.form__control'));
	controls.push(opt.view.$el.find('textarea.form__control'));
	controls.push(opt.view.$el.find('option:checked'));
	controls.push(opt.view.$el.find('input:checked'));

	function getName(el) {
		var n = el.getAttribute('name');
		if (n) {
			attrs[n] = el.value;
		} else {
			return getName(el.parentNode);
		}
	}

	_.each(controls, function(control) {
		_.each(control, function(el) {
			getName(el);
		})
	})

	opt.view.model.save(attrs, {
		success: function(collection, response) {
			if (response.status === 'ok') {
				$('.message-error').hide();
				window.location.hash = opt.nextUrl;
			} else if (response.status === 'error') {
				$('.message-error').html(response.message).show();
			}
			else {
				$('.message-error').html('Возникли проблемы при сохранении. Пожалуйста, повторите попытку.').show();
			}
		},
		error: function() {
			$('.message-error').html('Сервер временно недоступен. Пожалуйста, повторите попытку чуть позже.').show();
		}		
	});

	App.clientInfo.save();
}

App.Views.Menu = Backbone.View.extend({
	el: '.js-menu',
	template: template('menu'),
	render: function(route) {
		var hash = '#'+route;

		this.$el.html(this.template({active: hash, menu: this.collection.toJSON()}));

		App.viewMenuControl.render({
			route: route,
			control: this.collection.findWhere({url: hash}).toJSON().control
		});

		return this;
	},
});

App.Views.MenuControl = Backbone.View.extend({
	el: '#container',
	className: '.imodal__footer',
	template: template('menu-control'),
	render: function(options) {
		this.activeRoute = options.route;
		this.control = options.control;
		this.$el.find(this.className).html(this.template({control: options.control}));
	},
	events: {
		'submit': 'send'
	},
	send: function(e) {
		e.preventDefault();

		switch (this.activeRoute) {
			case 'service':
				send({
					view: App.viewServices,
					nextUrl: this.control.next.url,
				});
				break;
			case 'salon':
				send({
					view: App.viewSalons,
					nextUrl: this.control.next.url,
				});
				break;
			case 'timetable':
				send({
					view: App.viewTimetable,
					nextUrl: this.control.next.url,
				});
				break;
			case 'comments':
				send({
					view: App.viewComments,
					nextUrl: this.control.next.url,
				});
				break;
			case 'result':
				//App.viewResult.send();
		}
	}
});

App.Views.Services = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('services'),
	initialize: function() {
		var self = this,
			m = self.model;

		m.services.fetch({
			success: function(collection, response, options) {
				_.each(response, function(model) {
					m.questions[model.id] = new App.Collections.Questions;
					m.questions[model.id].url += model.id;
				})

				self.render();
			},
			error: function(collection, response, options) {
				console.log('error');
			}
		})
	},
	render: function() {
		this.$el.html(this.template({services: this.model.services.toJSON()}));

		//Инициализируем зависимые виды.
		App.viewQuestions = new App.Views.Questions();
		App.viewDuration = new App.Views.Duration();
		App.viewServiceLink = new App.Views.ServiceLink();

		this.$el.find('.js-select').chosen({
			disable_search_threshold: 1000,
			no_results_text: 'Не найдено',
		}).trigger('change');

		return this;
	},
	events: {
		'change .js-service-select': 'changed',
	},
	changed: function(el) {
		var id = $(el.target).find('option:checked').val(),
			detail = this.model.services.get(id).get('detail_link'); //detail service
			c = this.model.questions[id]; //Collection questions

		//Questions
		if (_.isEmpty(c.models)) {
			c.fetch({
				success: function(collection, response, options) {
					App.viewQuestions.render(collection);
				}
			})
		} else {
			App.viewQuestions.render(c);
		}

		//Duration
		App.duration = [];
		App.duration.push(this.model.services.get(id).get('duration'));
		App.viewDuration.render(App.duration);

		//Detail link service
		App.viewServiceLink.render(detail);

		App.clientInfo.set('services', this.model.services.get(id).get('name'));
		App.clientInfo.set('cost', this.model.services.get(id).get('cost'));
	},
})

App.Views.Questions = Backbone.View.extend({
	el: '#container .js-questions',
	render: function(collection) {
		var self = this;

		self.collection = collection;
		self.$el.html('');

		_.each(self.collection.models, function(m) {
			var question = new App.Views.Question({model: m});
			self.$el.append(question.render().el);
		})
	},
});

App.Views.Question = Backbone.View.extend({
	className: 'form__group',
	template: template('question'),
	render: function() {
		this.$el.html(this.template({question: this.model.toJSON()}));

		return this;
	},
	events: {
		'change': 'changed'
	},
	changed: function() {
		var id = this.$el.find('input:checked').val(),
			i = this.$el.index() + 1; //Первое значение - услуга, поэтому сдвигаемся на 1.
			doc  = this.model.toJSON().answers[id].doctor ? this.model.toJSON().answers[id].doctor : '';

		this.model.set('answer_check', id);

		App.duration[i] = this.model.toJSON().answers[id].duration;
		App.viewDuration.render(App.duration);

		App.clientInfo.set('doctor', doc); 
	}
});

App.Views.ServiceLink = Backbone.View.extend({
	className: '.js-detail-link',
	template: template('detail-link'),
	render: function(detail) {
		$(this.className).html(this.template({detailLink: detail}));
	}
});

App.Views.Duration = Backbone.View.extend({
	className: '.js-duration',
	template: template('duration'),
	render: function(arr) {
		var d = Math.max.apply(null, arr);

		$(this.className).html(this.template({duration: d}));

		App.clientInfo.set('duration', d);
	}
});

App.Views.Salons = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('salon'),
	templateDetail: template('salon-detail'),
	initialize: function() {
		var self = this;

		self.model.fetch({
			success: function(collection, response, options) {
				self.model.clear();
				if (response.is_held !== undefined) {
					self.model.is_held = response.is_held;
				}

				self.model.salons = new App.Collections.Salons(response.salons);
				self.render();
			},
			error: function() {
				console.log('error');
			}
		});
	},
	render: function() {
		this.$el.html(this.template({salons: this.model.salons.toJSON()}));

		$('.js-select').chosen({
			disable_search_threshold: 1000,
			no_results_text: 'Не найдено',
		});

		//Метки на карте
		map(this.model.salons);
	},
	events: {
		'change .js-salon-select' : 'changed'
	},
	changed: function(el) {
		var id = $(el.target).find('option:checked').val(),
			salon = this.model.salons.get(id),
			coordinate = this.model.salons.get(id).get('coordinate');

		this.$('.js-salon-detail').html(this.templateDetail({salon: salon.toJSON()}));

		App.myMap.setCenter(coordinate);
		App.clientInfo.set('salon', this.model.salons.get(id).get('address'));
	},
});

App.Views.SalonDetail = Backbone.View.extend({
	el: '.js-salon-detail',
	template: template('timetable'),
	render: function(salon) {
		this.$el.html(this.template({salon: salon.toJSON()}));
	}
});

App.Views.Timetable = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('timetable'),
	initialize: function() {
		this.render();
	},
	render: function() {
		console.log(App.clientInfo);
		this.$el.html(this.template({info: App.clientInfo.toJSON()}));

		App.viewTime = new App.Views.Time({collection: new App.Collections.Time()});
		App.viewCalendar = new App.Views.Calendar({collection: App.days});
	}
})

App.Views.Calendar = Backbone.View.extend({
	el: '.js-calendar',
	template: template('calendar'),
	inputDate: null,
	days: null,
	carousel: null,
	initialize: function() {
		this.render();
	},
	render: function() {
		var self= this;

		this.$el.html(this.template({calendar: this.collection.toJSON()}));
		this.inputDate = this.$el.find('.form__control--date');
		this.days = this.$el.find('.calendar__day');
		this.carousel = this.$el.find('.calendar__viewport');

		//Carousel
		this.carousel.jcarousel();
		$('.calendar__control--prev')
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				target: '-=6'
			});
		$('.calendar__control--next')
			.on('jcarouselcontrol:active', function() {
				$(this).removeClass('inactive');
			})
			.on('jcarouselcontrol:inactive', function() {
				$(this).addClass('inactive');
			})
			.jcarouselControl({
				target: '+=6'
			});

		//Mask
		this.inputDate.inputmask('dd.mm.yyyy', {
			yearrange: { minyear: self.getDate().slice(6)},
			radixPoint: '.'
		});

		App.viewTime.render(this.getDate());
	},
	getDate: function() {
		return this.collection.findWhere({active: true}).get('date');
	},
	events: {
		'click .calendar__day' : 'changeDay',
		'change .form__control--date' : 'changeDay',
	},
	changeDay: function(e) {
		var $target = $(e.target),
			prevDate,
			dayActive,
			date;

		if (e.type === 'click') {
			date = this.collection.at($target.index());
		}

		if (e.type === 'change') {
			date = this.collection.findWhere({date: $target.val()});
		}

		if (date) {
			prevDate = this.collection.findWhere({active: true}).set({active: false});
			$(this.days[this.collection.indexOf(prevDate)]).removeClass('calendar__day--active')
			
			date.set({active : true});
			dayActive = $(this.days[this.collection.indexOf(date)]).addClass('calendar__day--active');
			this.inputDate.val(date.get('date'));
			App.viewTime.render(date.get('date'));

			//Перематываем карусель если выбранный день за пределами видимости
			if (! this.carousel.jcarousel('visible').filter(dayActive).length) {
				this.carousel.jcarousel('scroll', dayActive.index());
			}

			$('.message-error').hide();
		} else {
			$('.message-error').html('На введенную вами дату ('+ $target.val() +') - запись не доступна.').show();
			this.inputDate.val(this.getDate());
		}
	},
});

App.Views.Time = Backbone.View.extend({
	el: '.js-time',
	template: template('time'),
	inputTime: null,
	listTime: null,
	render: function(day) {
		var self = this;

		self.collection.url = '/time.php'; //TODO delete

		self.collection.fetch({
			data: {day: day},
			success: function(collection, response, options) {
				self.collection.findWhere({disable: false}).set({active: true});
				self.$el.html(self.template({time: self.collection.toJSON()}));

				self.inputTime = self.$el.find('.form__control--time');
				self.listTime = self.$el.find('.time__el');

				self.inputTime.inputmask('9{2}:9{2}');
			},
			error: function(collection, response, options) {
				console.log('error');
			}
		});
	},
	events: {
		'click .time__el:not(.time__el--disable)' : 'changeTime',
		'change .form__control--time' : 'changeTime'
	},
	getTime: function() {
		return this.collection.findWhere({active: true}).get('from');
	},
	changeTime: function(e) {
		var $target = $(e.target),
			time,
			prevTime;

		if (e.type === 'click') {
			time = this.collection.at($target.index());
		}

		if (e.type === 'change') {
			time = this.collection.findWhere({from : $target.val(), disable: false});
		}

		if (time) {
			prevTime = this.collection.findWhere({active: true}).set({active: false});
			$(this.listTime[this.collection.indexOf(prevTime)]).removeClass('time__el--active')
			
			time.set({active : true});
			$(this.listTime[this.collection.indexOf(time)]).addClass('time__el--active')
			this.inputTime.val(time.get('from'));
			$('.message-error').hide();
		} else {
			$('.message-error').html('На введенное вами время ('+ $target.val() +') - запись не доступна.').show();
			this.inputTime.val(this.getTime());
		}
	}
});

App.Views.Comments = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('comments'),
	initialize: function() {
		this.$el.html(this.template());
	},
	render: function() {
		this.$el.html(this.template());
	},
});

App.Views.Result = Backbone.View.extend({
	el: '#container .imodal__body',
	template: template('result'),
	render: function() {
		var self = this;

		self.model.fetch({
			success: function(model, response, options) {
				self.$el.html(self.template({result: model.toJSON()}));
			},
			error: function() {
				self.$el.html(self.template());
				console.log('error');
			}
		})
	},
});

//MENU
App.viewMenuControl = new App.Views.MenuControl();
App.viewMenu = new App.Views.Menu({collection: App.menu});

//RESULT
App.viewResult = new App.Views.Result({model: App.result});
