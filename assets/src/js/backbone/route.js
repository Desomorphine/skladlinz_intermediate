App.Router = Backbone.Router.extend({
	routes: {
		'service'       : 'service',
		'salon'     	: 'salon',
		'timetable'     : 'timetable',
		'comments'      : 'comments',
		'result'        : 'result',
	},
	service: function() {
		if (App.viewServices === undefined) {
			App.viewServices = new App.Views.Services({model: new App.Models.Services()});
		} else {
			App.viewServices.render();
		}
	},
	salon: function() {
		if (App.viewSalon === undefined) {
			App.salons = new App.Models.Salons();
			App.viewSalons = new App.Views.Salons({model: App.salons});		
		} else {
			App.viewSalon.render();
		}	
	},
	timetable: function() {
		if (App.viewTimetable === undefined) {
			App.viewTimetable = new App.Views.Timetable({model: App.timetable});
		} else {
			App.viewTimetable.render();
		}
	},
	comments: function() {
		if (App.viewComments === undefined) {
			App.comments = new App.Models.Comments();
			App.viewComments = new App.Views.Comments({model: App.comments});
		} else {
			App.viewComments.render();
		}
	},
	result: function() {
		App.viewResult.render();
	},
})

App.route = new App.Router();
App.route.on('route', function(route) {
	App.viewMenu.render(route);
})
Backbone.history.start();