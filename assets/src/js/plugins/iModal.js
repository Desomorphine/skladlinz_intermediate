(function($) {
	var hepler = {
		/**
		 * [getViewportSize description]
		 * @return {[array]} [ [w] - width,
		 *                     [h] - height
		 *                   ]
		 */
		getViewportSize: function() {
			var a = document.documentElement,
				d = document.body,
				b = document.compatMode == 'CSS1Compat',
				c = window.opera,
				w = b && c ? window.innerWidth : b && !c ? a.clientWidth : d.clientWidth,
				h = b && c ? window.innerHeight : b && !c ? a.clientHeight : d.clientHeight,
				s = [];
				s['w'] = w;
				s['h'] = h;
			return s;			
		},
		/**
		 * [getDocumentHeight description]
		 * @return {[number]} [description]
		 */
		getDocumentHeight: function() {
			return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, this.getViewportSize()['h']);
		},
		/**
		 * [getScrollWidth description]
		 * @return {[number]} [description]
		 */
		getScrollWidth: function() {
			var outer = document.createElement('div');
			outer.style.visibility = 'hidden';
			outer.style.width = '100px';
			document.body.appendChild(outer);

			var widthNoScroll = outer.offsetWidth;
			outer.style.overflow = 'scroll';

			var inner = document.createElement('div');
			inner.style.width = '100%';
			outer.appendChild(inner);

			var widthWithScroll = inner.offsetWidth;
			outer.parentNode.removeChild(outer);

			return widthNoScroll - widthWithScroll;
		}
	};

	function IModal(element, options) {
		this.init(element);
	};

	IModal.prototype.viewportSize = hepler.getViewportSize()
	IModal.prototype.documentHeight = hepler.getDocumentHeight()
	IModal.prototype.scrollWidth = hepler.getScrollWidth()

	IModal.prototype.init = function(element) {
		var self = this;

		self._modal = $(element.data('modal'));
		self._modalBox = self._modal.find('.imodal__box');
		self._btnClose = self._modal.find('.js-imodal-close');

		element.on(
			'click',
			function(e) {
				self.show();
			});

		self._btnClose.on(
			'click',
			function(e) {
				self.close();

				e.preventDefault();
			});

		self._modal.on(
			'click',
			function() {
				self.close();
			});

		self._modalBox.on(
			'click',
			function(e) {
				e.stopPropagation();
			})
	};
	IModal.prototype.show = function() {
		this._modal.addClass('show');

		if (this.documentHeight > this.viewportSize['h']) {
			$('body').css({
				'paddingRight': this.scrollWidth,
				'overflow': 'hidden',
			});
		}
	};
	IModal.prototype.close = function() {
		this._modal.removeClass('show');

		$('body').css({
			'paddingRight' : 0,
			'overflow': 'auto',
		});
	};

	$.fn.iModal = function(options) {
		return this.each(function(options) {
			new IModal($(this), options);
			return this;
		});
	};	
})(jQuery);