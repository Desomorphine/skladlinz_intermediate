module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		concat: {
			dev: {
				src: [
					'assets/src/js/plugins/*.js',
					'assets/src/js/plugins.js',
					'assets/src/js/main.js',
					],
				dest: 'assets/js/main.js',
			},
			backbone:  {
				src: [
					'assets/src/js/backbone/multi-geocoder.js',
					'assets/src/js/backbone/localStorage.js',
					'assets/src/js/backbone/init.js',
					'assets/src/js/backbone/models.js',
					'assets/src/js/backbone/collections.js',
					'assets/src/js/backbone/views.js',
					'assets/src/js/backbone/route.js',
					],
				dest: 'assets/js/backbone.js',
			}
		},

		uglify: {
			dev: {
				files: [{
					expand: true,
					src: 'assets/js/main.js',
					dest: '',
					ext: '.min.js',
				}]
			},
			backbone: {
				files: [{
					expand: true,
					src: 'assets/js/backbone.js',
					dest: '',
					ext: '.min.js',
				}]
			}
		},

		watch: {
			dev: {
				files: ['assets/src/scss/**', 'assets/src/js/**'],
				tasks: ['concat', 'uglify', 'compass', 'copy']
			}
		},

		imagemin: {
			png: {
				options: {
					optimizationLevel: 7
				},
				files: [
					{
						expand: true,
						cwd: 'assets/img/',
						src: ['**/*.png'],
						dest: 'assets/img/',
						ext: '.png'
					}
				]
			},
			jpg: {
				options: {
					progressive: true
				},
				files: [
					{
						expand: true,
						cwd: 'assets/img/',
						src: ['**/*.jpg'],
						dest: 'assets/img/',
						ext: '.jpg'
					}
				]
			}
		},

		compass: {
			dev: {
				options: {
					sassDir: 'assets/src/scss/',
					cssDir: 'assets/css/',
					outputStyle: 'compressed',
					imageDir: 'assets/img',
					imagesPath: 'assets/src/img',
					generatedImagesDir: 'assets/img',
					generatedImagesPath: 'assets/img',
				}
			}
		},

		copy: {
			main: {
				files: [
					{
						expand: true, 
						cwd: 'assets/src/img/bg/',
						src: ['**'],
						dest: 'assets/img/bg/', 
					}
				]
			},
		},

		clean: {
			clean: ["assets/css", "assets/js", "assets/img"]
		}

	});

	grunt.registerTask('default', ['concat', 'uglify', 'compass', 'copy', 'watch:dev']);
	grunt.registerTask('build', ['clean', 'concat', 'uglify', 'compass', 'copy', 'imagemin']);
};